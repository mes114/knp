

getCHELSA <- function(lonLat, varname, 
                      path  = "/Users/jimclark/makeMastOnJimClark/makeMast/climateBuild/terraClimateFiles"){
  
  if(varname == 'ppt')varname <- 'prec'
  
 # one file per yr_month
  cpath <- paste( path,'/CHELSA',varname,'/',sep='')
  
  files <- list.files(path = cpath, 
                      pattern='.tif$', all.files=T, full.names=T)
  files <- files[ grep( paste('_', varname, sep=''), files) ]
  
  library(raster)
  #library(exifr)
  
  #data <- stack(files)
  
  mmat <- numeric(0)
  mcol <- character(0)
  
  for(m in 1:length(files)){  # one file per yr_month
    
    ym <- columnSplit(files[m], paste(varname,'_',sep=''))[,2]
    ym <- columnSplit(ym, '_V1.')[,1]
    mcol <- c(mcol, ym)
    
    tmp <- extract( raster( files[m], band = 1 ), y = lonLat)
    if(varname %in% c('tmin','tmax'))tmp <- tmp/10 - 273.15
    mmat <- cbind(mmat, tmp)
  }
  colnames(mmat) <- mcol
  rownames(mmat) <- rownames(lonLat)
  mmat
}


getTileTerraClimate <- function(lonLat, years, months = 1:12, vars = 'tmax', 
                                DOWNSCALE = T,
                                path = "/Users/jimclark/makeMastOnJimClark/makeMast/climateBuild/terraClimateFiles"){
  
  # plot is a character vector for each row in lonLat
  
  calibYears <- 2010:2013
  allYears <- sort(unique( c(years, calibYears) ) )
  
  if(is.null(rownames(lonLat))){
    rownames(lonLat) <- paste('s',1:nrow(lonLat))
  }
  site <- rownames(lonLat)
  
  ll <- round(lonLat, -1)
  ll <- columnPaste(ll[,1], ll[,2], '_' )
  colnames(lonLat) <- c('lon','lat')
  
  monNames <- as.character(months)
  monNames[ nchar(monNames) == 1] <- paste('0', monNames[ nchar(monNames) == 1], sep='' )
  
  icols <- c(1, 2, months + 2)
  
  kdat <- wideFormat <- numeric(0)
  
  for(k in 1:length(vars)){
    
    print(vars[k])
    
    if(k > 1){
      kdat <- cbind(kdat, z = NA)
      colnames(kdat)[ncol(kdat)] <- vars[k]
    }
    xwide <- numeric(0)
    
    for(j in 1:length(allYears)){
      
      yrm <- paste(allYears[j],monNames,sep='_')
      
      kpath <- paste(path, vars[k], allYears[j], ll, sep='/')
      kpath <- paste(kpath, '.rdata', sep = '')
      km    <- unique(kpath)
      
      for(i in 1:length(km)){
        
        if(!file.exists(km[i])){
          print('missing file: ')
          print(km[i])
          next
        }
        
        load(km[i])
        
        wi  <- which(kpath == km[i])
        xi  <- as.matrix(x[, c('lon','lat')])
        qi  <- lonLat[drop=F,wi,]
        ij  <- RANN::nn2( data = xi, query = qi)[[1]][,1]
        xyi <- x[ij,icols]                                          # wide format
        rownames(xyi) <- site[wi]
        xyi[,1:2] <- lonLat[wi,]
        if(DOWNSCALE){
          if(j == 1){
            xwide <- rbind(xwide, xyi)
          }else{
            if(i == 1){
              xnew <- matrix(NA, nrow(xwide), 12)
              colnames(xnew) <- yrm
              rownames(xnew) <- rownames(xwide)
              xwide <- cbind(xwide, xnew)
            }
            xwide[rownames(xyi),yrm] <- xyi[,yrm]
          }
        }
      }
    }
    
    if(DOWNSCALE){
      LL    <- lonLat[rownames(xwide),]
      
      cdata <- getCHELSA(LL, vars[k], path)
      cdata <- cdata[,colnames(cdata) %in% colnames(xwide)]
      cyr   <- sort(unique( as.numeric(columnSplit(colnames(cdata), '_')[,1]) ))
      cpr   <- colnames(xwide)[!colnames(xwide) %in% colnames(cdata) ][-c(1,2)]
      
      monC <- columnSplit(colnames(cdata),'_')[,2]
      monT <- columnSplit(colnames(xwide)[-c(1:2)],'_')[,2]
      
      # replace overlapping years with cdata, extend cdata with xwide
      if( max(years) > max(cyr) ){
        
        for(i in 1:length(site)){
          cy  <- cdata[i,]
          xx  <- data.frame( cdat = unlist(xwide[i,colnames(cdata)]), month = as.factor(monC) )
          
          fit <- lm( cy ~ cdat*month, data = xx )
          xn  <- unlist( xwide[i,-c(1:2)] )
          xn  <- data.frame( cdat = xn, month = as.factor(monT) )
          yp  <- predict.lm(fit, newdata = xn )
          xwide[i,names(yp)] <- signif(yp, 3)
          xwide[i, colnames(cdata)] <- cdata[i,]
        }
        
      }else{
        xwide[, colnames(cdata)] <- cdata
      }
      xwide <- xwide[rownames(lonLat),]
      
      kcols <- as.numeric( columnSplit(colnames(xwide)[-c(1,2)])[,1] )
      kcols <- c(colnames(xwide)[1:2], kcols[ kcols %in% years ])
      
      wideFormat <- append(wideFormat, list(xwide))
    }
  }
  names(wideFormat) <- vars
 # rownames(kdat) <- NULL
  
  nc   <- ncol(wideFormat[[1]]) - 2
  site <- rep(rownames(wideFormat[[1]]), each = nc)
  lon  <- rep(wideFormat[[1]]$lon, each = nc)
  lat  <- rep(wideFormat[[1]]$lat, each = nc)
  yr_m <- rep( colnames(wideFormat[[1]])[-c(1:2)], nrow(wideFormat[[1]]) )
  tallFormat <- data.frame( site, lon, lat, yr_m,
                            stringsAsFactors = F)
  for(k in 1:length(vars)){
    xk <- as.vector(unlist(t(wideFormat[[k]][,-c(1:2)])) )
    tallFormat <- cbind(tallFormat, xk)
    colnames(tallFormat)[ncol(tallFormat)] <- vars[k]
  }
  
  list( tallFormat = tallFormat, wideFormat = wideFormat )
}

tileTerraClimate <- function(vars = c('tmax', 'tmin'), 
                             path = "/Users/jimclark/makeMastOnJimClark/makeMast/climateBuild/terraClimateFiles"){
  
  path <- "/Users/jimclark/makeMastOnJimClark/makeMast/climateBuild/terraClimateFiles"
  
  require(ncdf4)
  monNames <- as.character(1:12)
  monNames[ nchar(monNames) == 1] <- paste('0', monNames[ nchar(monNames) == 1], sep='' )
  
  for(k in 1:length(vars)){
    
    xstring <- paste('_',vars[k], '_', sep='')
    files <- list.files( path, full.names = T)
    files <- files[ grep( xstring, files ) ]
    years <- columnSplit(files,  xstring)[,2]
    years <- sort( as.numeric( .replaceString(years, '.nc', '') ) )
   # data <- numeric(0)
    
    for(j in 1:length(years)){
      
      jfile <- files[ grep( paste(xstring, years[j], sep=''), files ) ]
      
      print(jfile)
      
      ncin <- nc_open( jfile )
      x   <- ncvar_get(ncin, vars[k])
      xk  <- numeric(0)
      
      for(m in 1:12){
        xm  <- as.vector(x[,,m])
        xk  <- cbind(xk, xm)
      }
      
      if(j == 1){
        lon <- ncvar_get(ncin, "lon")
        lat <- ncvar_get(ncin, "lat")
        fxy <- expand.grid(lon, lat)
        colnames(fxy) <- c('lon','lat')
        ix <- which(rowSums(xk, na.rm=T) != 0 &
                      fxy[,2] < 75 & 
                      fxy[,2] > -50)
        fxy <- fxy[ix,]
        
        tile <- round(fxy, -1)
        tile <- columnPaste(tile[,1], tile[,2], '_')
        tiles <- sort(unique(tile))
        
        mm <- match(tile, tiles)
      }
      xk <- xk[ix,]
      colnames(xk) <- paste( years[j], monNames, sep='_' )
      
      ipath <- paste( path, '/',vars[k], '/', years[j], '/', sep='')
      if(!dir.exists(ipath))dir.create(ipath)
      
      for(i in 1:length(tiles)){
        
        ifile <- paste( ipath, tiles[i], '.rdata', sep='')
        wi <- which(mm == i)
        x <- cbind(fxy[wi,],xk[wi,])
        save(x, file = ifile)
      }
    }
  }
}
  

getTerraClimate <- function( months, years, vars = c('tmin','tmax','pet','ppt'), 
                             xy = NULL, xlim = NULL, ylim = NULL, 
                             path = "/Users/jimclark/makeMastOnJimClark/makeMast/climateBuild/terraClimateFiles"){
  
  if(is.null(xy) & is.null(xlim))
    stop( 'must supply points xy or limits xlim, ylim' )
  
  require(ncdf4)
  
  if(!is.null(xy))colnames(xy) <- c('lon','lat')
  
  monNames <- as.character(months)
  monNames[ nchar(monNames) == 1] <- paste('0', monNames[ nchar(monNames) == 1], sep='' )
  
  id <- NULL
  
  if(!is.null(xy)){
    iname <- rep(1:nrow(xy), each = length(months))
    id <- rownames(xy)
  }
  
  files <- list.files( path, full.names = T)
  clim  <- numeric(0)
  
  for(y in 1:length(years)){
    
    yfiles <- files[ grep( paste(years[y], '.nc', sep=''), files ) ]
    yn <- numeric(0)
    
    for(k in 1:length(vars)){
      
      kfile <- yfiles[ grep( paste( '_', vars[k], '_',sep=''), yfiles ) ]
      
      print(kfile)
      
      ncin <- nc_open( kfile )
      
      if(k == 1 & y == 1){  # dimensions do not change
        
        lon <- ncvar_get(ncin, "lon")
        lat <- ncvar_get(ncin, "lat")
        fxy <- expand.grid(lon, lat)
        colnames(fxy) <- c('lon','lat')
        
        if(!is.null(xy)){                         # closest point
          ixy <- RANN::nn2( fxy, xy, k = 1)[[1]]
          nrep <- nrow(xy)
        }else{                                    # rectangle
          ixy <- which(fxy[,1] >= xlim[1] & fxy[,1] <= xlim[2] &
                         fxy[,2] >= ylim[1] & fxy[,2] <= ylim[2] )
          iname <- rep(1:length(ixy), each = length(months))
          nrep  <- length(ixy)
        }
        ff <- fxy[ixy[iname],]
        rownames(ff) <- NULL
      }
      
      if(k == 1){
        yn <- paste( years[y], monNames, sep='_')
        yn <- rep(yn, nrep)
        ss <- rep(id, each = length(monNames))
        yn <- data.frame(id = ss, year_month = yn, ff, stringsAsFactors = F)
      }
      
      x   <- ncvar_get(ncin, vars[k])
      xk  <- numeric(0)
      
      for(m in months){
        xm  <- as.vector(x[,,m])
        xk  <- cbind(xk, xm[ixy])
      }
      xk <- as.vector( t(xk) )
      yn <- cbind(yn, xk)
      colnames(yn)[ncol(yn)] <- vars[k]
      #      close( ncin )
    }
    clim <- rbind(clim, yn)
  }
  clim
}

climateProject <- function(climVec, PREC = F){
  
  # climVec is a yr_mo vector of climate data, one row of a siteXyr_mo matrix
  # the last values of climVec have NA and need to be filled
  
  nc <- length(climVec)
  nf <- max( which(is.finite(climVec)) )
  n.ahead <- nc - nf
  
  if(n.ahead == 0)return(climVec)
  
  fitAR <-  arima(climVec, order = c(2, 0, 0), 
                  seasonal = list(order = c(1, 0, 0), period = 12))
  ptmp  <- predict(fitAR, n.ahead = n.ahead)
  ptmp  <- ptmp$pred + rnorm(n.ahead, 0, ptmp$se) 
  if(PREC)ptmp[ptmp < 0] <- 0
  climVec[(nf+1):nc] <- ptmp
  round( climVec, 1 )
}

getPRISM <- function(xy, years, xvar = 'ppt', country = 'USA',
                     gpath='/nas/clark/clark.unix/GEEtables/final/'){
  
  # PRISM data for locations xy
  # xy has rownames = plot, colnames = c('LON','LAT')
  
  if(is.null(rownames(xy)))rownames(xy) <- c(1:nrow(xy))
  
  geePath <- paste(gpath, 'Grid4kmUSAcont/out/',sep='')
  if(country == 'CANADA')
    geePath <- paste(gpath, 'Grid4kmCanada/out/',sep='')
  
  nyr   <- length(years)
  nplot <- nrow(xy)
  
  mseq <- as.character(1:12)
  mseq[nchar(mseq) == 1] <- paste('0',mseq[nchar(mseq) == 1],sep='')
  
  ym <- rep(years, each=12)
  
  ym  <- columnPaste( ym, rep(mseq, nyr), sep='_')
  ym  <- rep(ym, nplot)
  rr  <- columnPaste( rep(rownames(xy), each = (12*nyr)), ym )
  
  xmat <- matrix(NA, length(ym), length(xvar))
  colnames(xmat) <- xvar
  
  template <- data.frame(plot = rep(rownames(xy), each = (12*nyr)),
                         yr_m = ym,
                         LON = rep(xy$LON, each = (12*nyr)),
                         LAT = rep(xy$LAT, each = (12*nyr)),
                         xmat, stringsAsFactors = F)
  rownames(template) <- rr
  
  xnames <- xvar
                          
  for(j in 1:length(xvar)){
    
    files <- list.files(geePath, pattern=xvar[j])
    tf <- files[grep('_PR.csv', files)]
    if(length(tf) == 0)tf <- files[grep('_TC.csv',files)]
    files <- tf
    if(length(files) == 0 & xvar[j] == 'ppt'){
      xvar[j] <- 'pr'
      files <- list.files(geePath, pattern=xvar[j])
      tf <- files[grep('_TC.csv',files)]
      files <- tf
      colnames(template)[colnames(template) == 'ppt'] <- 'pr'
    }
    if(length(files) == 0 & xvar[j] == 'tmin'){
      xvar[j] <- 'tmmn'
      files <- list.files(geePath, pattern=xvar[j])
      tf <- files[grep('_TC.csv',files)]
      files <- tf
      colnames(template)[colnames(template) == 'tmin'] <- 'tmmn'
    }
    if(length(files) == 0 & xvar[j] == 'tmax'){
      xvar[j] <- 'tmmx'
      files <- list.files(geePath, pattern=xvar[j])
      tf <- files[grep('_TC.csv',files)]
      files <- tf
      colnames(template)[colnames(template) == 'tmax'] <- 'tmmx'
    }
    
    ptmp <- columnSplit(files, paste(xvar[j],'_',sep=''))[,2]
    ptmp <- as.numeric( columnSplit(ptmp, paste('_monthly',sep=''))[,1] )
    wff  <- which(ptmp %in% years)
    
    files <- files[wff]
    
    tfiles <- paste(geePath, files, sep='')
    
    for(k in 1:length(files)){
      
      print(files[k])
      
      ym <- paste(years[k],mseq,sep='_')
      ym <- rep(ym, nrow(xy))
      
      tmp <- read.csv(tfiles[k], stringsAsFactors = F)
      wk  <- which(is.finite(tmp[,xvar[j]]) & !is.na(tmp$yr_m))
      if(length(wk) == 0)next
      
      indx <- RANN::nn2(tmp[,c('LON','LAT')], xy, k=1)$nn.idx
      jndx <- tmp$gee[indx]
      gee  <- tmp[tmp$gee %in% jndx,]
      
      ttt <- rep(jndx, each = 12)
      tdx <- columnPaste( ttt, ym )
      rtt <- columnPaste( rep(rownames(xy), each=12), ym )
      
      rownames(gee) <- columnPaste(gee$geeID, gee$yr_m)
      
      template[rtt,xvar[j]] <- gee[tdx,xvar[j]]
    }
  }
  
  mc <- match(xvar,colnames(template))
  colnames(template)[mc] <- xnames
  
  rownames(template) <- NULL
  template
}


smooth.na <- function(x,y){   
  
  #remove missing values
  #x is the index
  #y is a matrix with rows indexed by x
  
  if(!is.matrix(y))y <- matrix(y,ncol=1)
  
  wy <- which(!is.finite(y),arr.ind =T)
  if(length(wy) == 0)return(cbind(x,y))
  wy <- unique(wy[,1])
  ynew <- y[-wy,]
  xnew <- x[-wy]
  
  return(cbind(xnew,ynew))
}


split.screenMatrix <- function(mfrow, xpos, ypos, pnames){
  
  #setup plots for split.screen
  
  pcol <- mfrow[2]
  prow <- mfrow[1]
  
  lonq <- seq(0, 1, length.out=(pcol+1))
  latq <- seq(0, 1, length.out=prow)
  lonp <- quantile(xpos, lonq)
  lonp[pcol+1] <- max(lonp) + 1
  scol <- findInterval(xpos, lonp)
  scol[scol == (pcol+1)] <- pcol
  names(scol) <- pnames
  srow <- scol*0
  
  for(j in 1:pcol){       # up/down
    wj   <- which(scol == j)
    js <- names(wj[order(ypos[wj])])
    cc <- c(1:length(js))
    if(length(js) < prow){
      latp <- quantile(ypos, latq)
      ck <- findInterval(ypos[wj], latp)
      ws <- which(duplicated(ck))
      if(length(ws) > 0){
        latp <- quantile(ypos[wj], latq)
        ck <- findInterval(ypos[wj], latp)
      }
    }
    srow[js] <- cc
  }
  # rbind(srow,scol)
  
  xloc <- seq(0, 1, length.out=(1+pcol))
  yloc <- seq(0, 1, length.out=(1+prow))
  
  xp <- cbind(xloc[scol], xloc[scol+1])
  yp <- cbind(yloc[srow], yloc[srow+1])
  
  dx <- .3/pcol
  dy <- .3/prow
  
  xp[,1] <- xp[,1] + dx
  # xp[,2] <- xp[,2] - dx
  yp[,1] <- yp[,1] + dy
  # yp[,2] <- yp[,2] - dy
  
  xyplot <- cbind(xp, yp)
  rownames(xyplot) <- pnames
  xyplot
}



.getPlotLayout <- function(np){
  
  # np - no. plots
  
  if(np == 1)return( list( mfrow=c(1,1), left=1, bottom=c(1,2) ) )
  if(np == 2)return( list( mfrow=c(1,2), left = 1, bottom = c(1,2) ) )
  if(np == 3)return( list( mfrow=c(1,3), left = 1, bottom = c(1:3) ) )
  if(np <= 4)return( list( mfrow=c(2,2), left = c(1, 3), bottom = c(3:4) ) )
  if(np <= 6)return( list( mfrow=c(2,3), left = c(1, 4), bottom = c(4:6) ) )
  if(np <= 9)return( list( mfrow=c(3,3), left = c(1, 4, 7), bottom = c(7:9) ) )
  if(np <= 12)return( list( mfrow=c(3,4), left = c(1, 5, 9), bottom = c(9:12) ) )
  if(np <= 16)return( list( mfrow=c(4,4), left = c(1, 5, 9, 13), 
                            bottom = c(13:16) ) )
  if(np <= 20)return( list( mfrow=c(4,5), left = c(1, 6, 11, 15), 
                            bottom = c(15:20) ) )
  if(np <= 25)return( list( mfrow=c(5,5), left = c(1, 6, 11, 15, 20), 
                            bottom = c(20:25) ) )
  if(np <= 25)return( list( mfrow=c(5,6), left = c(1, 6, 11, 15, 20, 25), 
                            bottom = c(25:30) ) )
  return( list( mfrow=c(6,6), left = c(1, 7, 13, 19, 25, 31), bottom = c(31:36) ) )
}


.getColor <- function(col,trans){
  
  # trans - transparency fraction [0, 1]
  
  tmp <- col2rgb(col)
  rgb(tmp[1,], tmp[2,], tmp[3,], maxColorValue = 255, 
      alpha = 255*trans, names = paste(col,trans,sep='_'))
}


.shadeInterval <- function(xvalues,loHi,col='grey',PLOT=T,add=T,
                           xlab=' ',ylab=' ', xlim = NULL, ylim = NULL, 
                           LOG=F, trans = .5){
  
  #draw shaded interval
  
  tmp <- smooth.na(xvalues,loHi)
  
  xvalues <- tmp[,1]
  loHi <- tmp[,-1]
  
  xbound <- c(xvalues,rev(xvalues))
  ybound <- c(loHi[,1],rev(loHi[,2]))
  if(is.null(ylim))ylim <- range(as.numeric(loHi))
  if(is.null(xlim))xlim <- range(xvalues)
  
  if(!add){
    if(!LOG)plot(NULL, xlim = xlim, ylim=ylim, 
                 xlab=xlab, ylab=ylab)
    if(LOG)suppressWarnings( plot(NULL,  xlim = xlim, ylim=ylim, 
                                  xlab=xlab, ylab=ylab, log='y') )
  }
  
  
  if(PLOT)polygon(xbound,ybound, border=NA,col=.getColor(col, trans))
  
  invisible(cbind(xbound,ybound))
  
}


getFridley <- function(vname='TMAX', cname=colPK, FUN='mean'){
  
  file <- paste('fridley/',vname,'.csv',sep='')
  tmp <- read.csv(file,header=T)[,-1]
  
  date <- columnSplit(tmp$date, '-')
  yr   <- as.numeric(date[,1])
  mon  <- as.numeric(date[,2])
  
  wk <- which(cname %in% colnames(tmp))
  xx <- rowMeans(tmp[,cname[wk], drop=F], na.rm=T)
  
  wf <- which(is.finite(xx))
  
  inn <- list(year = yr[wf], month = mon[wf])
  tmp <- tapply(xx[wf], inn, FUN)
  
  mmat <- matrix(NA, nrow(tmp), 12)
  colnames(mmat) <- 1:12
  rownames(mmat) <- rownames(tmp)
  mmat[,colnames(tmp)] <- tmp
  
  signif(mmat, 3)
}

num2Month <- function(monthNum){
  
  mNames   <- c('jan','feb','mar','apr','may','jun','jul','aug',
                'sep','oct','nov','dec')
  mNames[monthNum]
}

month2Num <- function(monthName){
  
  match(monthName, num2Month(1:12) )
}

lowerFirstLetter <- function(xx){
  s <- unlist(strsplit(xx, " "))
  s <- paste(tolower(substring(s, 1, 1)), substring(s, 2),
             sep = "", collapse = " ")
  unlist(strsplit(s, " "))
}

columnSplit <- function(vec, sep='_', ASFACTOR = F, ASNUMERIC=F,
         LASTONLY=F){
  
  vec <- as.character(vec)
  nc  <- length( strsplit(vec[1], sep, fixed=T)[[1]] )
  
  mat <- matrix( unlist( strsplit(vec, sep) ), ncol=nc, byrow=T )
  if(LASTONLY & ncol(mat) > 2){
    rnn <- mat[,1]
    for(k in 2:(ncol(mat)-1)){
      rnn <- columnPaste(rnn,mat[,k])
    }
    mat <- cbind(rnn,mat[,ncol(mat)])
  }
  if(ASNUMERIC){
    mat <- matrix( as.numeric(mat), ncol=nc )
  }
  if(ASFACTOR){
    mat <- data.frame(mat)
  }
  if(LASTONLY)mat <- mat[,2]
  mat
}

trimRows <- function(mat){
  
  wc <- suppressWarnings( apply(mat, 1, min, na.rm=T) )
  mat[is.finite(wc),]
}
  
##############################################



getGEE <- function(getSite, vname = 'temp'){
  
  SITE <- T
  if(length( grep('_', getSite) ) == 1)SITE <- F
  
  if(vname == 'temp')vname <- c('tmin','tmax')
  if(vname[1] == 'prec' & !'prec' %in% colnames(geeClim)) vname <- 'ppt'
  if(vname[1] == 'pet')  vname <- 'eto'
  
  ww <- which(colnames(geeClim) == 'geeID')
  if(length(ww) > 0)colnames(geeClim)[ww] <- 'plot'
  
  if(!'site' %in% colnames(geeClim)){
    tmp <- columnSplit(geeClim$plot, '_')
    site <- tmp[,1]
    plot <- tmp[,2]
    geeClim$site <- site
  }
  
  geeClim$plot <- as.character(geeClim$plot)
  geeClim$site <- as.character(geeClim$site)
  
 # isplot <- grep('_', getSite)
  
  ws  <- which(geeClim$plot == getSite)
  
  if(length(ws) == 0)ws  <- which(geeClim$plot == getSite)
  
  if(length(ws) == 0) stop('site or plot not in geeClim$plot')
  
  tmp <- geeClim[drop=F, ws,]
    
  plot <- tmp$plot
  
  yrmo <- columnSplit(tmp$yr_m, '_')
  yr <- as.numeric(yrmo[,1])
  mo <- as.numeric(yrmo[,2])
  
  yvec <- c(min(yr):max(yr))
  nyr  <- length(yvec)
  
  yvar <- tmp[,vname, drop=F]
  if(ncol(yvar) > 1)yvar <- signif( rowMeans(yvar, 3 ) )
  yvar <- unlist(yvar)
  
  ym <- matrix(NA, nyr, 12)
  ym[ cbind( match(yr, yvec), mo ) ] <- yvar
  colnames(ym) <- 1:12
  rownames(ym) <- yvec
  ym
}


calibrateClim <- function(refmat, localmat, years, pmin=-Inf){
  
  # refmat and localmat have 12 columns for months, and one row per year
  # years are rownames
  
  colnames(refmat) <- colnames(localmat) <- 1:12
  refmat   <- as.matrix(refmat)
  localmat <- as.matrix(localmat)
  monthmat <- matrix(1:12, nrow(refmat), 12, byrow=T)
  rownames(monthmat) <- rownames(refmat)
  
  wy <- intersect(rownames(refmat),rownames(localmat))
  
  ltmp <- localmat[wy,]
  rtmp <- refmat[wy,]
  mtmp <- monthmat[wy,]
  
  xx <- as.vector( t(rtmp) )
  yy <- as.vector( t(ltmp) )
  mm <- as.vector( t(mtmp) )
  mm <- as.factor(mm)
  
  ww <- which(is.finite(xx) & is.finite(yy))
  xx <- xx[ww]
  yy <- yy[ww]
  mm <- mm[ww]
  
  form <- as.formula(yy ~ xx + mm)
  xd   <- model.matrix(form)
  
  FULL <- T
  
  rank <- qr(xd)$rank
  if(rank < ncol(xd)){
    FULL <- F
    form <- as.formula(yy ~ xx)
    xd   <- model.matrix(form)
  }
  
#  bg  <- solve(crossprod(xd))%*%crossprod(xd, yy)
  
  lfit <- lm(form)
  
  # predict missing from localmat
  
  wr <- which(is.finite(refmat))
  
  newdat <- data.frame(xx = refmat[wr], mm = as.factor(monthmat[wr]) )
  lpred  <- round( predict(lfit, newdata = newdat), 1)
  lpred[lpred < pmin] <- pmin
  
  refmat[wr] <- lpred
  
  rtmp <- refmat[wy,]
  rtmp[is.finite(ltmp)] <- ltmp[is.finite(ltmp)]
  
  refmat[wy,] <- rtmp
  refmat <- refmat[rownames(refmat) %in% years,]
  
  ww <- which(!rownames(localmat) %in% rownames(refmat))
  if(length(ww) > 0){
    tmp <- rbind(localmat[ww,], refmat)
    refmat <- tmp[order(rownames(tmp)),]
  }
  
  round(refmat,1)
  
  
}


.interp <- function(y,INCREASING=F,minVal=-Inf,maxVal=Inf,defaultValue=NULL,
                    tinySlope=NULL, hugeSlope=NULL){  #interpolate vector x
  
  if(is.null(defaultValue))defaultValue <- NA
  
  tiny <- .00001
  if(!is.null(tinySlope))tiny <- tinySlope
  
  y[y < minVal] <- minVal
  y[y > maxVal] <- maxVal
  
  n  <- length(y)
  wi <- which(is.finite(y))
  
  if(length(wi) == 0)return(rep(defaultValue,n))
  if(length(wi) == 1)ss <- tiny
  
  xx  <- c(1:n)
  z  <- y
  
  if(wi[1] != 1) wi <- c(1,wi)
  if(max(wi) < n)wi <- c(wi,n)
  
  ss <- diff(z[wi])/diff(xx[wi])
  
  ss[is.na(ss)] <- 0
 # ss[ss > hugeSlope] <- hugeSlope
  
  if(length(ss) > 1){
    if(length(ss) > 2)ss[1] <- ss[2]
    ss[length(ss)] <- ss[length(ss)-1]
  }
  if(INCREASING)ss[ss < tiny] <- tiny
  
  if(is.na(y[1]))  z[1] <- z[wi[2]] - xx[wi[2]]*ss[1]
  if(z[1] < minVal)z[1] <- minVal
  if(z[1] > maxVal)z[1] <- maxVal
  
  for(k in 2:length(wi)){
    
    ki <- c(wi[k-1]:wi[k])
    yk <- z[wi[k-1]] + (xx[ki] - xx[wi[k-1]])*ss[k-1]
    yk[yk < minVal] <- minVal
    yk[yk > maxVal] <- maxVal
    z[ki] <- yk
  }
  z
}

.interpRows <- function(x, startIndex=rep(1,nrow(x)), endIndex=rep(ncol(x),nrow(x)),
                        INCREASING=F, minVal=-Inf, maxVal=Inf,
                        defaultValue=NULL,tinySlope=.001){  
  #interpolate rows of x subject to increasing
  
  nn  <- nrow(x)
  p  <- ncol(x)
  xx <- c(1:p)
  
  if(length(minVal) == 1)minVal <- rep(minVal,nn)
  if(length(maxVal) == 1)maxVal <- rep(maxVal,nn)
  
  ni   <- rep(NA,nn)
  flag <- numeric(0)
  
  z <- x
  
  for(i in 1:nn){
    if(startIndex[i] == endIndex[i]){
      z[i,-startIndex[i]] <- NA
      next
    }
    z[i,startIndex[i]:endIndex[i]] <- .interp(x[i,startIndex[i]:endIndex[i]],
                                              INCREASING,minVal[i],maxVal[i],
                                              defaultValue,tinySlope)
  }
  
  z
}


mastClimFormat <- function(mat, site){
  
  tmp <- matrix( t(mat), 1)
  rownames(tmp) <- site
  
  ys <- rep(rownames(mat), each=12)
  ms <- rep(1:12, nrow(mat))
  colnames(tmp) <- paste(ys, ms, sep='_')
  tmp
}

writeMastFile <- function(mlist, fname){
  
  mm <- numeric(0)
  for(j in 1:length(mlist)){
    mj <- matrix(mlist[[j]], 1)
    colnames(mj) <- colnames(mlist[[j]])
    mm <- rbind(mm, mj)
  }
  mm <- signif(mm, 3)
  rownames(mm) <- names(mlist)
 # colnames(mm)
  write.csv(mm, file = fname,  quote = F)
}


climRAWS <- function(climdata, years){
  
  ny   <- length(years)
  
  climdata <- as.data.frame(climdata)
  monthnames <- num2Month(1:12)
  
  climdata$date <- .replaceString(climdata$date, ' ','')
  
  tmp <- columnSplit(climdata$date, '/')
  climdata$mon  <- as.numeric(tmp[,1])
  climdata$year <- as.numeric(tmp[,2])
  
  climdata <- climdata[climdata$year %in% years,]
  
  temp <- matrix(NA, ny, 12)
  colnames(temp) <- monthnames
  rownames(temp) <- years
  wy   <- match(climdata$year, years)
  wm   <- match(climdata$mon, 1:12)
  
  tmin <- tmax <- prec <- temp
  
  temp[ cbind(wy, wm) ] <- climdata$temp
  tmin[ cbind(wy, wm) ] <- climdata$tmin
  tmax[ cbind(wy, wm) ] <- climdata$tmax
  prec[ cbind(wy, wm) ] <- climdata$prec
  
  list(temp = trimRows(temp), tmin = trimRows(tmin), 
       tmax = trimRows(tmax), prec = trimRows(prec))
}
  
  
  
climNC <- function(climdata, years){        #convert NC data files
  
  ny   <- length(years)
  
  climdata <- as.data.frame(climdata)
  monthnames <- num2Month(1:12)
  
  climdata[,'mon'] <- lowerFirstLetter(as.character(climdata[,'mon']))
  
  wc       <- which(climdata[,'year'] >= min(years) & 
                      climdata[,'year'] <= max(years))
  climdata <- climdata[wc,]
  
  climdata[,'tempF']  <- (climdata[,'tempF'] - 32)*5/9         #to degrees C
  climdata[,'precInches'] <- 25.4* climdata[,'precInches']     #to mm 
  
  tmat <- matrix(NA,ny,12)
  rownames(tmat) <- years
  wc   <- match(climdata[,'mon'], monthnames)
  wr   <- match(climdata[,'year'],rownames(tmat))
  tmat[cbind(wr,wc)] <- round(climdata[,'tempF'],1)
  
  pmat <- matrix(NA,ny,12)
  rownames(pmat) <- years
  pmat[cbind(wr,wc)] <- round(climdata[,'precInches'],1)
  
  colnames(tmat) <- colnames(pmat) <- monthnames
  
  list(temp = trimRows(tmat), prec = trimRows(pmat) )
}

yearMonthVecCens <- function(yrvec){   
  
  #returns days to end of each month for years in yrvec
  
  allYears <- sort(unique(yrvec))
  nyr <- length(allYears)
  
  mNames   <- c('jan','feb','mar','apr','may','jun','jul','aug',
                'sep','oct','nov','dec')
  mDays <- rep(31,12)
  mDays[c(4,6,9,11)] <- 30
  mDays[2] <- 28
  names(mDays) <- mNames
  endMonth <- cumsum(rep(mDays,times=nyr))
  endYr    <- cumsum(rep(sum(mDays),nyr))
  list(endMonth = endMonth,endYr = endYr)
}


monthlyPET <- function(yi, mi, tempMatrix, precMatrix, lat){
  
  # m  = nyr*12 (serial months)
  # returns n X month*nyr matrix and n X nyr matrix 
  # tempMatrix - n X m location by month
  # precMatrix - n X m location by month
  # yi     - length-m year index  (e.g., 1990, 1991)
  # mi     - length-m month index (1:12)
  
  if(!is.matrix(tempMatrix))tempMatrix <- matrix(tempMatrix,1)
  if(!is.matrix(precMatrix))precMatrix <- matrix(precMatrix,1)
  
  n <- nrow(tempMatrix)
  m <- ncol(tempMatrix)
  yrvec <- min(yi):max(yi)
  yseq  <- yrvec - yrvec[1] + 1
  nyr   <- length(yrvec)
  ymat  <- matrix(yseq,n,nyr,byrow=T)
  
  yindex <- match(yi,yrvec)
  
  JD <- c(1:365)
  
  dl <- dayLength(JD,lat)
  mo <- daysSinceDate(1,1,yrvec[1],c(2:12),1,yrvec[1],nineteen=F)
  di <- findInterval(JD,mo) + 1
  
  yvec <- as.vector( matrix(yindex,n,m,byrow=T) )
  ivec <- as.vector( matrix(c(1:n),n,m) )
  
  # heat index by year
  tmp1   <- tempMatrix
  tmp1[tmp1 < 0] <- 0
 # HI    <- byRcpp(as.vector((tmp1/5)^1.514),ivec,yvec,ymat*0,ymat*0, fun='sum')
  
  index <- list(year = yvec, month = ivec)
  HI    <- tapply(as.vector((tmp1/5)^1.514), index, sum)
  
  
  HI    <- HI[yvec]
  alpha <- (6.75*1e-7)*HI^3 - (7.71*1e-5)*HI^2 + (1.792*1e-2)*HI + .49239
  
  tmp <- aggregateSequence(di,dl,action='mean')  #mean monthly daylength
  dlMon <- tmp[[1]]
  dlLen <- tmp[[2]]
  dlLen <- dlLen[,mi]
  
  PET     <- 16*dlLen/12*(10*tmp1/HI)^alpha
  list(PET = PET, daylength = dlLen)
}


dayLength <- function(JD,lat){
  
  nl  <- length(lat)
  nj  <- length(JD)
  latNew <- lat/180
  
  JDmat <- JD
  
  if(nl > 1 & nj > 1){
    latNew <- matrix(latNew,nl,nj)
    JDmat  <- matrix(JD,nl,nj,byrow=T)
  }
  
  p <- asin( .39795*cos(.2163108 + 2*atan(.9671396*tan(.0086*(JDmat - 186)))) )
  tmp <- 24 - (24/pi)*acos( (sin(.8333*pi/180) + sin(latNew*pi)*sin(p)) /
                              (cos(latNew*pi)*cos(p)))
  tmp
}


daysSinceDate <- function(month0,day0,yr0,mo,da,yr,nineteen=F){  #Julian dates from dates
  
  #vectors for mo, da, yr since initial date 0
  
  dd <- clark_mdy.date(mo,da,yr,nineteen)
  dd - clark_mdy.date(month0, day0, yr0,nineteen) + 1
}


clark_mdy.date <- function (month, day, year, 
                            nineteen = TRUE, fillday = FALSE, 
                            fillmonth = FALSE){
  
  #days since 1/1/1960
  
  temp <- any( (month != trunc(month)) | (day != trunc(day)) | 
                 (year != trunc(year)))
  if (!is.na(temp) && temp) {
    warning("Non integer input values were truncated in mdy.date")
    month <- trunc(month)
    day   <- trunc(day)
    year  <- trunc(year)
  }
  if (nineteen) 
    year <- ifelse(year < 100, year + 1900, year)
  temp  <- numeric(length(month + day + year))
  month <- month + temp
  day   <- day + temp
  year  <- year + temp
  if (fillmonth) {
    temp <- is.na(month)
    month[temp] <- 7
    day[temp]   <- 1
  }
  if (fillday) 
    day[is.na(day)] <- 15
  month[month < 1 | month > 12] <- NA
  day[day < 1] <- NA
  year[year == 0] <- NA
  year   <- ifelse(year < 0, year + 1, year)
  tyear  <- ifelse(month > 2, year, year - 1)
  tmon   <- ifelse(month > 2, month + 1, month + 13)
  julian <- trunc(365.25 * tyear) + trunc(30.6001 * tmon) + 
    day - 715940
  temp <- trunc(0.01 * tyear)
  save <- ifelse(julian >= -137774, 
                 julian + 2 + trunc(0.25*temp) - temp, 
                 julian)
  year   <- ifelse(month == 12, year + 1, year)
  month  <- ifelse(month == 12, 1, month + 1)
  day    <- 1
  tyear  <- ifelse(month > 2, year, year - 1)
  tmon   <- ifelse(month > 2, month + 1, month + 13)
  julian <- trunc(365.25 * tyear) + trunc(30.6001 * tmon) + day - 715940
  temp   <- trunc(0.01 * tyear)
  save2  <- ifelse(julian >= -137774, julian + 2 + trunc(0.25*temp) - temp, julian)
  temp   <- as.integer(ifelse(save2 > save, save, NA))
  temp
}

aggregateSequence <- function(index,dat,action='mean',minObs=NULL){    
  # index - column index for aggregation
  # dat is n by t
  
  nc <- length(index)
  if(!is.matrix(dat)){
    dat <- matrix(dat,ncol=nc)
  }
  nn <- nrow(dat)
  
  allDates <- sort(unique(index))
  nm <- length(allDates)
  
  newDat   <- matrix(NA,nn,nm)
  rownames(newDat) <- rownames(dat)
  colnames(newDat) <- allDates
  
  jk <- 0
  for(j in allDates){
    jk <- jk + 1
    wj <- which(index == j)
    mj <- dat[,wj]
    
    if(!is.null(minObs))if(length( which(is.finite(mj)) ) < minObs)next
    
    if(action == 'mean'){
      if(is.matrix(mj)) newDat[,jk] <- rowMeans(mj,na.rm=T)
      if(!is.matrix(mj))newDat[,jk] <- mean(mj,na.rm=T)
    }
    if(action == 'sum'){
      if(is.matrix(mj)) newDat[,jk] <- rowSums(mj,na.rm=T)
      if(!is.matrix(mj))newDat[,jk] <- sum(mj,na.rm=T)
    }
  }
  list(dates = allDates, data = newDat)
}



daysSinceDate <- function(month0,day0,yr0,mo,da,yr,nineteen=F){  #Julian dates from dates
  
  #vectors for mo, da, yr since initial date 0
  
  dd <- clark_mdy.date(mo,da,yr,nineteen)
  dd - clark_mdy.date(month0, day0, yr0,nineteen) + 1
}


clark_mdy.date <- function (month, day, year, 
                            nineteen = TRUE, fillday = FALSE, 
                            fillmonth = FALSE){
  
  #days since 1/1/1960
  
  temp <- any( (month != trunc(month)) | (day != trunc(day)) | 
                 (year != trunc(year)))
  if (!is.na(temp) && temp) {
    warning("Non integer input values were truncated in mdy.date")
    month <- trunc(month)
    day   <- trunc(day)
    year  <- trunc(year)
  }
  if (nineteen) 
    year <- ifelse(year < 100, year + 1900, year)
  temp  <- numeric(length(month + day + year))
  month <- month + temp
  day   <- day + temp
  year  <- year + temp
  if (fillmonth) {
    temp <- is.na(month)
    month[temp] <- 7
    day[temp]   <- 1
  }
  if (fillday) 
    day[is.na(day)] <- 15
  month[month < 1 | month > 12] <- NA
  day[day < 1] <- NA
  year[year == 0] <- NA
  year   <- ifelse(year < 0, year + 1, year)
  tyear  <- ifelse(month > 2, year, year - 1)
  tmon   <- ifelse(month > 2, month + 1, month + 13)
  julian <- trunc(365.25 * tyear) + trunc(30.6001 * tmon) + 
    day - 715940
  temp <- trunc(0.01 * tyear)
  save <- ifelse(julian >= -137774, 
                 julian + 2 + trunc(0.25*temp) - temp, 
                 julian)
  year   <- ifelse(month == 12, year + 1, year)
  month  <- ifelse(month == 12, 1, month + 1)
  day    <- 1
  tyear  <- ifelse(month > 2, year, year - 1)
  tmon   <- ifelse(month > 2, month + 1, month + 13)
  julian <- trunc(365.25 * tyear) + trunc(30.6001 * tmon) + day - 715940
  temp   <- trunc(0.01 * tyear)
  save2  <- ifelse(julian >= -137774, julian + 2 + trunc(0.25*temp) - temp, julian)
  temp   <- as.integer(ifelse(save2 > save, save, NA))
  temp
}


.replaceString <- function(xx,now='_',new=' '){  #replace now string in vector with new
  
  ww <- grep(now,xx,fixed=T)
  if(length(ww) == 0)return(xx)
  
  for(k in ww){
    s  <- unlist( strsplit(xx[k],now,fixed=T) )
    ss <- s[1]
    if(length(s) == 1)ss <- paste( ss,new,sep='')
    if(length(s) > 1)for(kk in 2:length(s)) ss <- paste( ss,s[kk],sep=new)
    xx[k] <- ss
  }
  xx
}

