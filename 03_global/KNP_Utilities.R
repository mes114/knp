# KNP_Utilities.R
# Created 09/23/19
# Author: Margaret Swift
# margaret.swift@duke.edu

# Holds all universal Utility functions, sources, libraries, 
# and constants for KNP Project

###############################################################################
# Libraries
###############################################################################
message('Loading packages...')
packages <- c('devtools', 'elevatr', 'foreign', 'geosphere', 'ggplot2', 'ggpubr', 
              'gjam', 'grid', 'gridExtra', 'here', 'kriging', 'lattice', 'lwgeom', 
              'MASS', 'radiant.data', 'RcppArmadillo', 'Rcpp', 'reshape2', 'rgeos', 'sf', 'sp')
suppressPackageStartupMessages({ pacman::p_load(char=packages) })
message('Packages loaded.')


###############################################################################
# Functions
###############################################################################
message('Loading KNP global functions...')

# Map Params
proj4string <- "+proj=longlat +zone=36 +ellps=WGS84 +datum=WGS84 +no_defs"
crs <- CRS(proj4string)

aggregateShapesToGrid <- function(shp, grid, types.df, desc.col) {
  # Map each grid square by %types
  # Right now I've made this the % of area w/in the park
  
  # Remove geometry for processing
  shp.nogeom <- st_set_geometry(shp, NULL)
  
  # loop over each row in shapefile to grab descriptions
  for (i in 1:nrow(shp)) {
    desc <- tolower(as.character(shp.nogeom[i, desc.col]))
    
    # map descriptions to types
    go.on <- TRUE
    for (j in 1:ncol(types.df)) {
      if (go.on) {
        types <- as.character(types.df[,j])
        type <- sum(sapply(types, function(x) grepl(x, desc)))
        if (type) {
          type <- names(types.df[j])
          go.on <- FALSE
        }
      }
    }
    
    # save types to shape
    shp$type[i] <- ifelse(type==0, NA, type)
  }
  
  types <- names(types.df)
  for (type in types) {
    grid[,type] <- 0
    my.shp <- st_union(shp[which(shp$type == type),])
    for (j in 1:nrow(grid)) {
      x <- pkgcond::suppress_messages(st_intersection(my.shp, grid$.[j]))
      x.a <- ifelse(length(x)>0, st_area(x), 0)
      grid[j,type] <- round(x.a / grid$shp_area_m2[j], 2) 
    }
  }
  return(grid)
}
createGrid <- function(shp, nlon=10, nlat=50, sizekm=1000, crs=crs, effort=10, latlon=T) {
  ## Create a grid of effort values, each being the fraction of area within the ##
  ## grid square that is contained within the underlying shape.##
  if (latlon) {
    grid_size <- c(nlon, nlat)
    grid <- st_make_grid(shp, n=grid_size) %>% st_sf(grid_id = 1:length(.))
  } else {
    # x <- attributes(shp$geometry)$bbox
    a_km <- as.numeric(st_area(shp)) / 1e6
    grid_size <- sizekm / a_km
    grid <- st_make_grid(shp, cellsize=grid_size) %>% st_sf(grid_id = 1:length(.))
  }
  grid$area_m2 <- st_area(grid$.)
  grid$area_km2 <- grid$area_m2/1e6
  
  # Assign effort to each square of grid, defined as the percentage
  # of each grid square that is contained in KNP
  L <- nrow(grid)
  for (i in 1:L) {
    x <- pkgcond::suppress_messages(st_intersection(st_geometry(shp), st_geometry(grid$.[i])))
    x.a <- st_area(x)
    if (!length(x.a)) x.a <- 0
    if (!length(x)) x <- 0
    grid$shp_int[i] <- x
    grid$shp_area_m2[i] <- x.a
  }
  grid$shp_area_pct <- round(as.numeric(grid$shp_area_m2 / grid$area_m2),4)
  grid <- grid[-which(grid$shp_area_m2 == 0),]
  return(grid)
}
deleteRow <- function(arr, inx) { return (arr[-c(inx), ]) }
distFrom <- function(arr,start=1) { 
  # Returns a vector measuring the distance of each point from a specified point
  # (default is first value in array)
  arr[,1] <- as.numeric(arr[,1])
  arr[,2] <- as.numeric(arr[,2])
  start_pt <- arr[start,]
  d <- distm(start_pt, arr, fun=distHaversine)
  return(t(d)) #transpose row to column
}
findAnoms <- function(knp.grid, type, inx.g, years) {
  inx.t <- which(grepl(type, names(knp.grid)))
  inx.y <- unlist(lapply(years, function(x) which(grepl(x,names(knp.grid[inx.t])))))
  rows <- knp.grid[i, inx.t]
  st_geometry(rows) <- NULL
  m <- mean(as.numeric(rows)) 
  anoms <- as.numeric(rows[inx.y] - m)
  return(anoms)
}
findMatches <- function(vec, df, col) {
  colvals <- tolower(df[,col])
  newdf <- df[0,]
  for (i in 1:length(vec)) {
    x <- tolower(vec[i])
    inx <- which(grepl(x, colvals))
    if (length(inx) > 0) {
      subdf <- df[inx,]
      subdf$SPECIES <- x
      newdf <- rbind(newdf, subdf)
    }
  }
  return(newdf)
}
findMinDist <- function(point, edges) {
  # st_cast(edges, "MULTILINESTRING")
  dist <- min(st_distance(point, edges))
  return(dist)
}
findDuplicateLatLon <- function(sp) {
  duplicates <- sp::zerodist(as(sp, 'Spatial'))
  return( duplicates[,2])
}
findYear <- function(str) {
  matches <- regmatches(str, gregexpr("[[:digit:]]+", str))
  return(as.numeric(unlist(matches)))
}
fixName <- function(n) {
  # Fixes up names from species descriptions
  n <- tolower(as.character(n))
  n <- gsub(" ", "_", n)
  n <- gsub("[[:punct:]]", "", n)
  return(n)
}
getByYear <- function(year, df) {
  inx <- which(grepl(year, rownames(df)))
  return (df[inx,])
}
getClark <- function() {
  if (isConnected()) { source(file.path(clarkdir,'clarkFunctions.R'))
  } else { warning("WARNING: Cannot connect to clark.unix") } 
}
getShape <- function(shp_name, p4s=proj4string) {
  ## Get a shape from a shapefile and transform into SF ##
  shp <- st_read(shp_name) %>% st_transform(p4s)
  return(shp)
}
getShapeBorder <- function(shp) {
  ## Get outside border from a conglomeration of polygons ##
  u <- shp[1,]
  for (i in 2:nrow(shp)) u <- st_union(u, shp[i,])
  u <- u$geometry
  return(u)
}
getTime <- function(t) {
  time <- ceiling(proc.time()['elapsed'] - t['elapsed'])
  if (time < 60) time <- paste(time, 'sec')
  else if ( time < 3600 ) time <- paste(ceiling(time/60), 'min')
  else time <- paste(round(time/3600, 2), 'hr')
  return(time)
}
isConnected <- function() { 
  return(ifelse(length(list.files(clarkdir)),T,F)) 
}
isShape <- function(file, returnCols=F) {
  lon.list <- c('lon', 'long', 'longitude', 'x_coord')
  lat.list <- c('lat', 'latitude', 'y_coord')
  cols <- tolower(colnames(file)) 
  bool <- any(cols %in% c(lon.list, lat.list, 'geometry'))
  bool <- any(bool, 'sf' %in% class(file))
  if (returnCols) {
    lon.col <- which(cols %in% lon.list)
    lat.col <- which(cols %in% lat.list)
    return(c(lon.col, lat.col))
  } else { return(bool) }
}
knpPath <- function(path='', isDataSet=F) { 
  dir <- here::here()
  datadir <- list.files(dir)[grepl('datasets', list.files(dir))]
  if (isDataSet) dir <- file.path(dir, datadir)
  return(file.path(dir, path)) 
}
knpPlot <- function(fill="transparent", color="black", grid=NULL) {
  mapfile <- paths$border
  g <- getShape(shp_name=mapfile)
  p <- ggplot() + 
    geom_sf(data=g$geometry, color=color, fill=fill) + 
    scale_x_continuous(breaks=c(31, 31.5, 32))
  if (!is.null(grid)) {
    p <- p + geom_sf(data=grid$., fill='transparent')
  }
  return(p)
}
krigData <- function(values.df, grid.df, s_p, col, xcol='X_COORD', ycol="Y_COORD") {
  cat('Preparing data...\n')
  require(geoR)
  t0 <- proc.time()
  # Pull column for this year, remove NA and <=0 rows
  colname <- names(values.df)[col]
  st_geometry(values.df) <- NULL
  w <- values.df[,col] # new copy for this year
  
  # Remove NA rows and make (-) values 0
  inx.na <- which(is.na(w))
  w <- w[-inx.na]
  values.df <- values.df[-inx.na,]
  w <- ifelse(w <= 0, 0, w)
  s <- cbind(values.df[,xcol], values.df[,ycol])
  t1 <- getTime(t0)
  cat('Data prepared (', t1, ')\n')
  
  # Fit model and catch errors
  cat("Fitting model...\n")
  t0 <- proc.time()
  fit  <- tryCatch({ 
    likfit(data=w, coords=s, fix.nugget=FALSE,
           cov.model="exponential", ini = c(var(w), 1), messages=F)
    
  }, error = function(e) { cat('ERROR WITH FITTING', colname, '\n'); print(e)
  })
  t1 <- getTime(t0)
  cat('Model fitted (', t1, ')\n')
  
  # Run kriging & catch errors
  
  if (!is.null(fit)) {
    cat("Kriging ...\n")
    t0 <- proc.time()
    pred <- tryCatch({ 
      krige.conv(data=w,
                 coords=s,locations=s_p,  
                 krige=krige.control(cov.model="exponential", 
                                     beta=fit$beta,
                                     cov.pars=fit$cov.pars, 
                                     nugget=fit$nugget))
    }, error = function(e) { cat('ERROR WITH KRIGING', colname, '\n'); print(e) 
    })
    t1 <- getTime(t0)
    cat('Kriging finished (', t1, ')\n')
  } else {
    cat('Kriging aborted.\n')
    pred <- NULL
  }
  
  # If kriging worked, give each grid square a value
  # that is the mean of the krigged veg points in that square.
  if (!is.null(pred)) {
    cat("Fitting predictions to grid squares ...\n")
    t0 <- proc.time()
    
    k <- data.frame(long=s_p[,1],lat=s_p[,2], pred=pred$predict,se=sqrt(pred$krige.var))
    sp <- sfFromLongLat(k,"long","lat")
    grid.df[,colname] <- 0
    for (i in 1:nrow(grid.df)) {
      grid <- grid.df$.[i]
      inx <- which(!is.na(over(as(sp$geometry,"Spatial"), as(grid,"Spatial"), fn=NULL)))
      grid.df[i,colname] <- mean(sp$pred[inx])
    }
    t1 <- getTime(t0)
    cat('Kriging finished. (', t1, ')\nFinished with', colname, '!\n\n')
  } else cat('Krig fitting aborted.\nProcessing for', colname, 'has failed.\n\n')
  return(grid.df)
}
makeTable <- function(df, countcol="TOTAL", itercol="YEAR", classcol="SPECIES") {
  iter <- unique(df[,itercol])
  cls <- unique(df[,classcol])
  tbl <- data.frame(matrix(0, nrow=length(iter), ncol=length(cls)))
  names(tbl) <- c(cls)
  row.names(tbl) <- iter
  for (i in 1:length(iter)) {
    for (j in 1:length(cls)) {
      inx <- intersect(which(df[,itercol] == iter[i]), which(df[,classcol] == cls[j]))
      tbl[i,j] <- sum(df[inx,countcol])
    }
  }
  return(tbl)
}
namesToCols <- function(df) {
  n <- row.names(df)
  n.ls <- unlist(strsplit(n, '_'))
  group <- as.numeric(n.ls[seq(2, length(n.ls), by=3)])
  time <- as.numeric(n.ls[seq(3, length(n.ls), by=3)])
  df <- cbind(group, time, df)
  names(df)[1:2] <- c('grid', 'year')
  df
}
plotShape <- function(data, long, lat, color="red") {
  sf <- st_as_sf(data, coords = c(long, lat), crs=crs)
  ggplot() + geom_sf(data=sf, aes(col=color, fill=color))
}
plotTrend <- function(spp, df.list) {
  tot.df <- findMatches(spp, df.list[[1]], 2)
  for (i in 2:length(df.list)) {
    match <- findMatches(spp, df.list[[i]], 2)
    tot.df <- rbind(tot.df, match)
  }
  tot.tbl <- makeTable(tot.df)
  years <- as.numeric(row.names(tot.tbl))
  melt.df <- data.frame(YEAR=years, melt(tot.tbl))
  names(melt.df) <- c("YEAR", "SPECIES", "TOTAL")
  p <- ggplot(data=melt.df, aes(x=YEAR, y=TOTAL, color=SPECIES)) + 
    geom_line() + scale_x_continuous(breaks=years) +
    theme(axis.text.x = element_text(angle = 90))
  p
}
readFile <- function(path, doGeom=T) {
  path <- as.character(path)
  end <- tolower(gsub('.*\\.', '', path))
  if ( end == 'shp' ) { return(invisible(getShape(path)))
  } else if ( end == 'csv' ) { file <- read.csv(path)
  } else if ( end == 'txt' ) { file <- read.table(path, header=T)
  } else if ( end == 'rdata' | end == 'rds' ) { file <- readRDS(path)
  } else if ( end == 'dbf' ) { file <- read.dbf(path) }
  else { stop(paste0('Path extension "', end,
                     '" not found. Cannot read file at this time.')) }
  
  # Return with coords if necessary
  makeShape <- !("geometry" %in% colnames(file))
  if (isShape(file) && makeShape && doGeom) {
    cols <- isShape(file, returnCols=T)
    loncol <- file[,cols[1]]
    latcol <- file[,cols[2]]
    file <- sfFromLongLat(file, cols[1], cols[2])
    file$lon <- loncol
    file$lat <- latcol
  }
  return(file)
}
sfFromLongLat <- function(data, long, lat, crs=crs) {
  sf <- st_as_sf(data, coords=c(long, lat), crs=crs)
  return(sf)
}
sfPointsInPoly <- function(points, sf, col) {
  # Find which polygon each point is in; set NA for edges
  insects <- st_intersects(points, sf, sparse = FALSE)
  inx <- vector(mode="numeric", length=nrow(insects))
  for (i in 1:nrow(insects)) {
    val <- which(insects[i,])
    inx[i] <- ifelse(!length(val), NA, val)
  }
  
  # Edge cases need to use distance; this is slower!!
  inx.na <- which(is.na(inx))
  dists <- st_distance(points$geometry[inx.na], sf)
  inx[inx.na] <- whichMin(dists)
  
  # Return assignments
  sf$geometry <- NULL
  result <- as.character(sf[inx,col])
  return(result)
}
simplifyClasses <- function(vec, mapping) {
  vec <- as.character(vec)
  if (isShape(vec)) vec$geometry <- NULL
  result <- vec
  for (i in 1:length(vec) ) {
    type  <- names(which(vec[i] == unlist(mapping)))
    result[i] <- gsub('[0-9]*', '', type)
  }
  return(result)
}
whichMin <- function(vec) {
  n <- nrow(vec)
  if (is.null(n)) {
    n <- 1
    vec <- matrix(data=vec, nrow=1)
  }
  m <- vector(mode="numeric", length=n)
  for ( i in 1:n ) {
    row <- vec[i,]
    m[i] <- which(row == min(row))
  }
  return(m)
}

#---------------------------------------------
#fixed clark functions:
columnPaste <- function(c1, c2, sep='-'){

  FACT <- T
  if(!is.factor(c1))FACT <- F
  c1    <- as.character(c1)
  c2    <- as.character(c2)
  c12   <- apply( cbind(c1, c2) , 1, paste0, collapse=sep)
  c12   <- replaceString(c12, ' ', '')
  if(FACT) c12 <- as.factor(c12)
  c12

}
replaceString <- function(xx,now='_',new=' '){#replace now string in vector with new
  
  ww <- grep(now,xx,fixed=T)
  if(length(ww) == 0)return(xx)
  
  for(k in ww){
    s  <- unlist( strsplit(xx[k],now,fixed=T) )
    ss <- s[1]
    if(length(s) == 1)ss <- paste( ss,new,sep='')
    if(length(s) > 1)for(kk in 2:length(s)) ss <- paste( ss,s[kk],sep=new)
    xx[k] <- ss
  }
  xx
}
message("KNP global functions loaded.")

###############################################################################
# Global Variables
###############################################################################
message("Loading KNP global variables...")

# clark.unix path based on OS
ismac <- ifelse(Sys.info()[['sysname']]=="Darwin", TRUE, FALSE)
if (ismac) { clarkdir <- "/Volumes/clark/clark.unix"
} else { clarkdir <- "R:/clark/clark.unix" }

# file paths
paths <- data.frame(
  'boreholes'= knpPath('boreholes/data/', T),
  'census'= knpPath('census/data/census_2011.shp', T),
  'distance'= knpPath('distance/data/all_census.shp', T),
  'dung'= knpPath('dung/data/judithk.111975.1-Kruger_VCA_dungcounts.xlsx', T),
  'eas'= knpPath('eas/data/raw/EAS_ungulates_1977_1997.RData', T),
  'ecozones'= knpPath('ecozones/data/ecozones.shp', T),
  'fire'= knpPath('fire/data/percentage_burned.xlsx', T),
  'geology'= knpPath('geology/data/geology_venter_1991.shp', T),
  'hippo'= knpPath('hippo/data', T),
  'croc'= knpPath('croc/data', T),
  'border'= knpPath('border/data/knpBorder.shp', T),
  'landscapes'= knpPath('landscapes/data/landscapes.shp', T),
  'rain'= knpPath('rain/data/DAILY RAINFALL1903-2007.csv', T),
  'rivers'= knpPath('rivers/data/rivers_main.shp', T),
  'soil'= knpPath('soil/data/soils_venter_1991.shp', T),
  'vca'= knpPath('vca/data/vca_grass_1989_2012.csv', T)
)

message("KNP global variables loaded.")

# EOF