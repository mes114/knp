# plotGrids.R
# Date: 3/19/2020
# Author: Margaret Swift
# Contact: margaret.swift@duke.edu

# Plot grids of all sorts.

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# SOURCE & SETUP

rm(list=ls())
setwd(dirname(rstudioapi::getSourceEditorContext()$path))
source('../../03_global/KNP_Utilities.R')
source('utilities.R')
load('data/xdata.RData')
load('data/ydata.RData')
load('data/gridData.RData')

p <- knpPlot(grid=knp.grid)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# MAIN

# Rare antelopes over time
years <- 1989:1996
sp <- c('e', 't', 'ro', 's')
mx <- c(max(ydata$e[ydata$year%in%years]), max(ydata$t[ydata$year%in%years]), 
        max(ydata$ro[ydata$year%in%years]), max(ydata$s[ydata$year%in%years]))
col <- list(e=c("#d2b2e5", "#281139"), t=c("#A6FAAA", "#2D7A30"), 
            ro=c("#B2CDFA", "#0B316F"), s=c("#F0C3E3", "#780657"))
for(i in 1:length(years)){
  year <- years[i]
  for (j in 1:length(sp)) {
    species <- c('eland', 'tsessebe', 'roan', 'sable')
    myname <- paste("~/Desktop/plots/", sp[j], "_", year, ".jpg", sep = "")
    mytitle <- paste(species[j], year)
    fill <- ydata[which(ydata$year==year), sp[j]]
    grid <- cbind(knp.grid, fill)
    q <- p + geom_sf(data=grid, aes(fill=fill), color='#f2f2f2') + 
      ggtitle(mytitle) +
      scale_fill_gradient(low = col[sp[j]][[1]][1], high = col[sp[j]][[1]][2], 
                          limits=c(0, mx[j]))
    jpeg(myname)
      print(q)
    dev.off()
  }
}


#eof