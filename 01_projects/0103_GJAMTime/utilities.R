# utilities.R
# Date: 2/13/2020
# Author: Margaret Swift
# Contact: margaret.swift@duke.edu

# Utilities file to hold GJAMTime and other functions that don't belong in global.

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Load files and packages

source('getLatestGJAM.R')
colF <- colorRampPalette( c('#8c510a','#d8b365','#c7eae5','#5ab4ac','#01665e','#2166ac') )

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# FUNCTIONS
message('loading my functions...')
reduce <- function(df, inx=NULL, by=0.01) {
  df[inx,] <- df[inx,] * by
  return(df)
}
runFilter <- function(dataset, fs=1.0) {
  # Run a filter on a datset to simulate missing data.
  # dataset <- datalist[[1]]$data
  
  # Fix filter if percent given as number & print msg
  if (1<fs & fs<=100) fs <- fs/100 
  message('Filtering dataset by ', fs*100, ' percent.')
  
  n <- nrow(dataset$xdata)
  inx <- 1:n
  n.keep <- floor(n * fs)
  inx.keep <- sort(sample(inx, n.keep))
  
  dataset$ydata <- reduce(dataset$ydata, inx.keep, 0.01)
  dataset$edata <- reduce(dataset$edata, inx.keep, 0.01)
  
  return(dataset)
}
segment <- function(data, chunks) {
  segments <- list()
  for (i in 1:nrow(chunks)) {
    row <- chunks[i,]
    seq <- row$start:row$end
    chunk <- data[which(data$times %in% seq),]
    segments[[i]] <- chunk
  }
  return(segments)
}
segmentData <- function(dataset, chunks=data.frame(start=c(1, 101), end=c(30, 130))) {
  datasets <- list(flux=dataset, eqm=dataset)
  xdata <- segment(dataset$xdata, chunks)
  datasets$flux$xdata <- xdata[[1]]
  datasets$eqm$xdata <- xdata[[2]]
  # more for ydata and edata
  return(datasets)
}

message('done')
