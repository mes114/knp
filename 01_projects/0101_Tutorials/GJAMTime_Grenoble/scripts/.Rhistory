diag(alpha) <- astar # intraspecific competition
# Initialize lo and hi alphas for filling in
loAlpha <- alpha
hiAlpha <- loAlpha*0
#..............................................................................
# Create some general interaction rules for diet types. These species are
# either browsers, grazers, or mixed feeders. The literature suggests little
# overlap in diet regime between two species of the same diet, due to height &
# niche differences, but we can assume at least a small bit of competition.
species <- colnames(ydata)
c <- -0.01 # let's be generous & say a 5% population decrease when competing.
# Run over each species; get name, diet, and max population, and find out
# who they could be competing with
for (i in 1:length(species)) {
sp <- species[i] #species name
sp.pop <- max(ydata[,sp]) #largest pop at one time
diet <- as.character( diets$diet[ diets$species==sp ] ) #species diet regime
# We will assume that mixed feeders are able to change their diet enough
# to mitigate interspecific same-regime competition.
if (diet != 'mixed') {
compete <- as.character(diets$species[ diets$diet == diet ])
compete <- compete[ compete != sp ] # intra- was already done.
for (j in which(species %in% compete))
{
# Effect of species j on species i should be at most c = 5%.
sp.prime <- species[j]
sp.prime.pop <- max(ydata[,sp.prime]) #largest pop at one time
# alpha * sp.pop * sp.prime.pop = c * sp.pop; solve for alpha
a <- c / sp.prime.pop
loAlpha[i,j] <- a # competition, so loAlpha < 0
hiAlpha[i,j] <- 0  # maybe there's some facilitation? who knows...
}
}
}
#........................................
# Manually adjust for known cooperations:
# (1) Wildebeest and zebra should work together.
kw <- which(species %in% c('k', 'w'))
loAlpha[kw, kw] <- 0
# (2) Elephants help facilitate feeding for some smaller animals:
fac <- which(species %in% c('v', 'r', 'wb', 'b', 'k', 'w'))
inx.o <- which(species == 'o')
loAlpha[inx.o, fac] <- loAlpha[fac, inx.o] <- 0
#.............................................
alphaPrior <- list(lo = loAlpha, hi = hiAlpha)
###############################################################################
# MODEL PARAMETERS
###############################################################################
# Effort matrix
effMat[ effMat == 0 ] <- 1
effort <- list(columns = 1:S, values = effMat)
# Time and Reduct Lists
timeList <- list(times = 'year', groups = 'grid',
alphaPrior = alphaPrior, betaPrior = betaPrior,
lambdaPrior = lambdaPrior )
rl <- list(N = 8, r = 5)
# Plotting: Color species by diet type.
specColor <- rep('black', ncol(ydata))
inx.g <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'grazer')])
inx.b <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'browser')])
specColor[inx.g] <- 'green'
specColor[inx.b] <- 'blue'
# Put it all together: modelList and plotPars
modelList <- list(typeNames = 'DA', ng=2000, burnin=500, reductList = rl,
timeList=timeList, effort = effort)
plotPars <- list(specColor = specColor, GRIDPLOTS=T, PLOTALLY=T, SAVEPLOTS=F,
CLUSTERPLOTS=T)
###############################################################################
# RUN THE MODEL
###############################################################################
output <- gjam(bet.form, xdata=xdata, ydata=ydata, modelList=modelList)
gjamPlot(output, plotPars)
# EOF
# gjamTime.R
# Created 11/17/19
# Author: Margaret Swift
# margaret.swift@duke.edu
# This file runs as a tutorial for GJAM Time Series using data collected at
#    Kruger National Park in South Africa. Response data are herbivore count
#    data from 1989 to 2012; covariates include precipitation, minimum temps,
#    soil and geology types, distance from water sources, and grass biomass.
# For more about GJAM, open the vignette: vignette('gjamVignette')
# For more about the Time Series version, see 'information/GJAMTime.html'.
# For more on the data used in this tutorial, see 'readme.txt' and
#    "information/KNP_Overview.pdf"
###############################################################################
# SETUP
###############################################################################
# Clean wd, source utils file, set booleans, set up files.
rm(list=ls())
source('utilities.R') # load utility and GJAM time series scripts
source('fixUpData.R') # pare down x-, y-, and edata for tutorial
###############################################################################
# Read in data
###############################################################################
diets <- read.csv('../data/species_diets.csv')
xdata <- readRDS('../data/xdata.RData')
ydata <- readRDS('../data/ydata.RData')
edata <- readRDS('../data/edata.RData')
###############################################################################
# Fill Missing Times
###############################################################################
# First, set 'times' and 'groups' to the appropriate column names; 'groupVars'
# are parameters that don't change over time.
times  <- 'year'
groups <- 'grid'
groupVars <- names(xdata)[!grepl('anom', names(xdata))]
# Fill in missing data for time series & add initialized time-0 row.
tmp <- gjamFillMissingTimes(xdata, ydata, edata, groups, times,
groupVars=groupVars, FILLMEANS=T,
typeNames='DA', missingEffort=0.1)
# Reset variables with the missing time fills.
xdata    <- tmp$xdata
ydata    <- tmp$ydata
timeZero <- tmp$timeZero
timeLast <- tmp$timeLast
rowInserts <- tmp$rowInserts
snames   <- colnames(ydata)
effMat   <- tmp$edata
# Fill initialized time-0 rows for xdata with means from the other years
cols <- which(is.na(xdata[1,]))
for (grid in unique(xdata$grid)) {
rows <- which(xdata$grid==grid)
xdata[rows[1],cols] <- colMeans(xdata[rows[-1],cols])
}
# Effort isn't changing over time for this one, so correct time-0 efforts
inx.0 <- which(grepl('-0',rownames(effMat)))
effMat[inx.0,] <- effMat[inx.0+1,]
###############################################################################
# SET PRIORS
###############################################################################
#------------------------------------------------------------------------------
# Beta Priors: Density-Independent Environment Effects ( B * x * x' )
#------------------------------------------------------------------------------
# We are trying to estimate the amount of em-/immigration into/out of each grid
# that is affected by each covariate. 'Lo' of 0 means we think it will have a
# positive effect. We leave 'hi' to be figured out by the data, except for 'dist
# from rivers', which we expect to have a negative effect on immigration; and
# 'anom.grass', which we expect to have a pretty strong effect (5% increase)
# on immigration.
bet.form <- formula(~ dist.rivers + anom.ppt + anom.mint + veld.sav)
lo <- list(anom.ppt       = 0,
anom.mint      = 0,
veld.sav       = 0)
hi <- list(dist.rivers    = 0)
b  <- gjamPriorTemplate(bet.form, xdata, ydata, lo=lo, hi=hi)
loBeta <- b[[1]]; hiBeta <- b[[2]]
loBeta[!is.finite(loBeta)] <- -0.5
hiBeta[!is.finite(hiBeta)] <- 0.5
loBeta['intercept',] <- hiBeta['intercept',] <- NA
#..............................................
betaPrior <- list(lo = loBeta, hi = hiBeta)
#------------------------------------------------------------------------------
# Lambda Priors: Density-Dependent Environment-Species Effects ( L * v * v' )
#------------------------------------------------------------------------------
# We are trying to estimate the amount of population growth that is affected by
# each covariate. We only include covariates in the lam.form if we think they
# have an effect. 'lo' of 0 means we think it will have a positive effect. 'hi'
# of 0 is a negative effect
lam.form <- as.formula(~ veld.sav + anom.ppt)
lo <- list(intercept=0.7); hi <- list(intercept=1.2)
g <- gjamPriorTemplate(lam.form, xdata, ydata, lo=lo, hi=hi)
loLambda <- g[[1]]; hiLambda <- g[[2]]
loLambda[!is.finite(loLambda)] <- -0.5
hiLambda[!is.finite(hiLambda)] <- 0.5
#..............................................
lambdaPrior <- list(lo = loLambda, hi = hiLambda)
#------------------------------------------------------------------------------
# Alpha Priors: Density-Independent Species Interactions ( A * w * w' )
#------------------------------------------------------------------------------
# Create an alpha matrix that is SxS for species interactions
S <- ncol(ydata)
alpha <- matrix(NA,S,S)
# Estimate carrying capacity of system (w*), assuming it is probably about
# 10x the maximum of: ( what we observe ) / ( effort )
wstar <- apply( ydata / effMat, 2, max, na.rm=T )
wstar <- wstar * 10
# Competition due to same species will be negative, but not more than 20%;
# therefore if we want to have a*w* = 80%, we need to make a* = 80% / w*.
astar <- (1-1.2) / wstar
z <- sqrt( crossprod( as.matrix(ydata) ) )
tmpMat <- matrix( astar, S, S, byrow=T )
alpha[z > 1000] <- 2 * tmpMat[z > 1000] # which species have huge effects?
diag(alpha) <- astar # intraspecific competition
# Initialize lo and hi alphas for filling in
loAlpha <- alpha
hiAlpha <- loAlpha*0
#..............................................................................
# Create some general interaction rules for diet types. These species are
# either browsers, grazers, or mixed feeders. The literature suggests little
# overlap in diet regime between two species of the same diet, due to height &
# niche differences, but we can assume at least a small bit of competition.
species <- colnames(ydata)
c <- -0.01 # let's be generous & say a 5% population decrease when competing.
# Run over each species; get name, diet, and max population, and find out
# who they could be competing with
for (i in 1:length(species)) {
sp <- species[i] #species name
sp.pop <- max(ydata[,sp]) #largest pop at one time
diet <- as.character( diets$diet[ diets$species==sp ] ) #species diet regime
# We will assume that mixed feeders are able to change their diet enough
# to mitigate interspecific same-regime competition.
if (diet != 'mixed') {
compete <- as.character(diets$species[ diets$diet == diet ])
compete <- compete[ compete != sp ] # intra- was already done.
for (j in which(species %in% compete))
{
# Effect of species j on species i should be at most c = 5%.
sp.prime <- species[j]
sp.prime.pop <- max(ydata[,sp.prime]) #largest pop at one time
# alpha * sp.pop * sp.prime.pop = c * sp.pop; solve for alpha
a <- c / sp.prime.pop
loAlpha[i,j] <- a # competition, so loAlpha < 0
hiAlpha[i,j] <- 0  # maybe there's some facilitation? who knows...
}
}
}
#........................................
# Manually adjust for known cooperations:
# (1) Wildebeest and zebra should work together.
kw <- which(species %in% c('k', 'w'))
loAlpha[kw, kw] <- 0
# (2) Elephants help facilitate feeding for some smaller animals:
fac <- which(species %in% c('v', 'r', 'wb', 'b', 'k', 'w'))
inx.o <- which(species == 'o')
loAlpha[inx.o, fac] <- loAlpha[fac, inx.o] <- 0
#.............................................
alphaPrior <- list(lo = loAlpha, hi = hiAlpha)
###############################################################################
# MODEL PARAMETERS
###############################################################################
# Effort matrix
effMat[ effMat == 0 ] <- 1
effort <- list(columns = 1:S, values = effMat)
# Time and Reduct Lists
timeList <- list(times = 'year', groups = 'grid',
alphaPrior = alphaPrior, betaPrior = betaPrior,
lambdaPrior = lambdaPrior )
rl <- list(N = 8, r = 5)
# Plotting: Color species by diet type.
specColor <- rep('black', ncol(ydata))
inx.g <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'grazer')])
inx.b <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'browser')])
specColor[inx.g] <- 'green'
specColor[inx.b] <- 'blue'
# Put it all together: modelList and plotPars
modelList <- list(typeNames = 'DA', ng=2000, burnin=500, reductList = rl,
timeList=timeList, effort = effort)
plotPars <- list(specColor = specColor, GRIDPLOTS=T, PLOTALLY=T, SAVEPLOTS=F,
CLUSTERPLOTS=T)
###############################################################################
# RUN THE MODEL
###############################################################################
output <- gjam(bet.form, xdata=xdata, ydata=ydata, modelList=modelList)
gjamPlot(output, plotPars)
dirname(sys.frame(1)$ofile)
# Clean wd, source utils file, set booleans, set up files.
rm(list=ls())
#setwd('My/Current/Directory') # you need to set your current directory.
source('utilities.R') # load utility and GJAM time series scripts
source('fixUpData.R') # pare down x-, y-, and edata for tutorial
diets <- read.csv('../data/species_diets.csv')
xdata <- readRDS('../data/xdata.RData')
ydata <- readRDS('../data/ydata.RData')
edata <- readRDS('../data/edata.RData')
View(xdata)
View(ydata)
# First, set 'times' and 'groups' to the appropriate column names; 'groupVars'
# are parameters that don't change over time.
times  <- 'year'
groups <- 'grid'
groupVars <- names(xdata)[!grepl('anom', names(xdata))]
# Fill in missing data for time series & add initialized time-0 row.
tmp <- gjamFillMissingTimes(xdata, ydata, edata, groups, times,
groupVars=groupVars, FILLMEANS=T,
typeNames='DA', missingEffort=0.1)
# Reset variables with the missing time fills.
xdata    <- tmp$xdata
ydata    <- tmp$ydata
timeZero <- tmp$timeZero
timeLast <- tmp$timeLast
rowInserts <- tmp$rowInserts
snames   <- colnames(ydata)
effMat   <- tmp$edata
head(ydata)
head(xdata)
# Fill initialized time-0 rows for xdata with means from the other years
cols <- which(is.na(xdata[1,]))
for (grid in unique(xdata$grid)) {
rows <- which(xdata$grid==grid)
xdata[rows[1],cols] <- colMeans(xdata[rows[-1],cols])
}
# Effort isn't changing over time for this one, so correct time-0 efforts
inx.0 <- which(grepl('-0',rownames(effMat)))
effMat[inx.0,] <- effMat[inx.0+1,]
effMat   <- tmp$edata
# We are trying to estimate the amount of em-/immigration into/out of each grid
# that is affected by each covariate. 'Lo' of 0 means we think it will have a
# positive effect. We leave 'hi' to be figured out by the data, except for 'dist
# from rivers', which we expect to have a negative effect on immigration; and
# 'anom.grass', which we expect to have a pretty strong effect (5% increase)
# on immigration.
bet.form <- formula(~ dist.rivers + anom.ppt + anom.mint + veld.sav)
lo <- list(anom.ppt       = 0,
anom.mint      = 0,
veld.sav       = 0)
hi <- list(dist.rivers    = 0)
b  <- gjamPriorTemplate(bet.form, xdata, ydata, lo=lo, hi=hi)
loBeta <- b[[1]]; hiBeta <- b[[2]]
loBeta[!is.finite(loBeta)] <- -0.5
hiBeta[!is.finite(hiBeta)] <- 0.5
loBeta['intercept',] <- hiBeta['intercept',] <- NA
#..............................................
betaPrior <- list(lo = loBeta, hi = hiBeta)
# have an effect. 'lo' of 0 means we think it will have a positive effect. 'hi'
# of 0 is a negative effect
lam.form <- as.formula(~ veld.sav + anom.ppt)
lo <- list(intercept=0.7); hi <- list(intercept=1.2)
g <- gjamPriorTemplate(lam.form, xdata, ydata, lo=lo, hi=hi)
loLambda <- g[[1]]; hiLambda <- g[[2]]
loLambda[!is.finite(loLambda)] <- -0.5
hiLambda[!is.finite(hiLambda)] <- 0.5
#..............................................
lambdaPrior <- list(lo = loLambda, hi = hiLambda)
# gjamTime.R
# Created 11/17/19
# Author: Margaret Swift
# margaret.swift@duke.edu
# This file runs as a tutorial for GJAM Time Series using data collected at
#    Kruger National Park in South Africa. Response data are herbivore count
#    data from 1989 to 2012; covariates include precipitation, minimum temps,
#    soil and geology types, distance from water sources, and grass biomass.
# For more about GJAM, open the vignette: vignette('gjamVignette')
# For more about the Time Series version, see 'information/GJAMTime.html'.
# For more on the data used in this tutorial, see 'readme.txt' and
#    "information/KNP_Overview.pdf"
###############################################################################
# SETUP
###############################################################################
# Clean wd, source utils file, set booleans, set up files.
rm(list=ls())
#setwd('My/Current/Directory') # you need to set your current directory.
source('utilities.R') # load utility and GJAM time series scripts
source('fixUpData.R') # pare down x-, y-, and edata for tutorial
###############################################################################
# Read in data
###############################################################################
diets <- read.csv('../data/species_diets.csv')
xdata <- readRDS('../data/xdata.RData')
ydata <- readRDS('../data/ydata.RData')
edata <- readRDS('../data/edata.RData')
###############################################################################
# Fill Missing Times
###############################################################################
# First, set 'times' and 'groups' to the appropriate column names; 'groupVars'
# are parameters that don't change over time.
times  <- 'year'
groups <- 'grid'
groupVars <- names(xdata)[!grepl('anom', names(xdata))]
# Fill in missing data for time series & add initialized time-0 row.
tmp <- gjamFillMissingTimes(xdata, ydata, edata, groups, times,
groupVars=groupVars, FILLMEANS=T,
typeNames='DA', missingEffort=0.1)
# Reset variables with the missing time fills.
xdata    <- tmp$xdata
ydata    <- tmp$ydata
timeZero <- tmp$timeZero
timeLast <- tmp$timeLast
rowInserts <- tmp$rowInserts
snames   <- colnames(ydata)
effMat   <- tmp$edata
# Fill initialized time-0 rows for xdata with means from the other years
cols <- which(is.na(xdata[1,]))
for (grid in unique(xdata$grid)) {
rows <- which(xdata$grid==grid)
xdata[rows[1],cols] <- colMeans(xdata[rows[-1],cols])
}
# Effort isn't changing over time for this one, so correct time-0 efforts
# inx.0 <- which(grepl('-0',rownames(effMat)))
# effMat[inx.0,] <- effMat[inx.0+1,]
###############################################################################
# SET PRIORS
###############################################################################
#------------------------------------------------------------------------------
# Beta Priors: Density-Independent Environment Effects ( B * x * x' )
#------------------------------------------------------------------------------
# We are trying to estimate the amount of em-/immigration into/out of each grid
# that is affected by each covariate. 'Lo' of 0 means we think it will have a
# positive effect. We leave 'hi' to be figured out by the data, except for 'dist
# from rivers', which we expect to have a negative effect on immigration; and
# 'anom.grass', which we expect to have a pretty strong effect (5% increase)
# on immigration.
bet.form <- formula(~ dist.rivers + anom.ppt + anom.mint + veld.sav)
lo <- list(anom.ppt       = 0,
anom.mint      = 0,
veld.sav       = 0)
hi <- list(dist.rivers    = 0)
b  <- gjamPriorTemplate(bet.form, xdata, ydata, lo=lo, hi=hi)
loBeta <- b[[1]]; hiBeta <- b[[2]]
loBeta[!is.finite(loBeta)] <- -0.5
hiBeta[!is.finite(hiBeta)] <- 0.5
loBeta['intercept',] <- hiBeta['intercept',] <- NA
#..............................................
betaPrior <- list(lo = loBeta, hi = hiBeta)
#------------------------------------------------------------------------------
# Lambda Priors: Density-Dependent Environment-Species Effects ( L * v * v' )
#------------------------------------------------------------------------------
# We are trying to estimate the amount of population growth that is affected by
# each covariate. We only include covariates in the lam.form if we think they
# have an effect. 'lo' of 0 means we think it will have a positive effect. 'hi'
# of 0 is a negative effect
lam.form <- as.formula(~ veld.sav + anom.ppt)
lo <- list(intercept=0.7); hi <- list(intercept=1.2)
g <- gjamPriorTemplate(lam.form, xdata, ydata, lo=lo, hi=hi)
loLambda <- g[[1]]; hiLambda <- g[[2]]
loLambda[!is.finite(loLambda)] <- -0.5
hiLambda[!is.finite(hiLambda)] <- 0.5
#..............................................
lambdaPrior <- list(lo = loLambda, hi = hiLambda)
#------------------------------------------------------------------------------
# Alpha Priors: Density-Independent Species Interactions ( A * w * w' )
#------------------------------------------------------------------------------
# Create an alpha matrix that is SxS for species interactions
S <- ncol(ydata)
alpha <- matrix(NA,S,S)
# Estimate carrying capacity of system (w*), assuming it is probably about
# 10x the maximum of: ( what we observe ) / ( effort )
wstar <- apply( ydata / effMat, 2, max, na.rm=T )
wstar <- wstar * 10
# Competition due to same species will be negative, but not more than 20%;
# therefore if we want to have a*w* = 80%, we need to make a* = 80% / w*.
astar <- (1-1.2) / wstar
z <- sqrt( crossprod( as.matrix(ydata) ) )
tmpMat <- matrix( astar, S, S, byrow=T )
alpha[z > 1000] <- 2 * tmpMat[z > 1000] # which species have huge effects?
diag(alpha) <- astar # intraspecific competition
# Initialize lo and hi alphas for filling in
loAlpha <- alpha
hiAlpha <- loAlpha*0
#..............................................................................
# Create some general interaction rules for diet types. These species are
# either browsers, grazers, or mixed feeders. The literature suggests little
# overlap in diet regime between two species of the same diet, due to height &
# niche differences, but we can assume at least a small bit of competition.
species <- colnames(ydata)
c <- -0.01 # let's be generous & say a 5% population decrease when competing.
# Run over each species; get name, diet, and max population, and find out
# who they could be competing with
for (i in 1:length(species)) {
sp <- species[i] #species name
sp.pop <- max(ydata[,sp]) #largest pop at one time
diet <- as.character( diets$diet[ diets$species==sp ] ) #species diet regime
# We will assume that mixed feeders are able to change their diet enough
# to mitigate interspecific same-regime competition.
if (diet != 'mixed') {
compete <- as.character(diets$species[ diets$diet == diet ])
compete <- compete[ compete != sp ] # intra- was already done.
for (j in which(species %in% compete))
{
# Effect of species j on species i should be at most c = 5%.
sp.prime <- species[j]
sp.prime.pop <- max(ydata[,sp.prime]) #largest pop at one time
# alpha * sp.pop * sp.prime.pop = c * sp.pop; solve for alpha
a <- c / sp.prime.pop
loAlpha[i,j] <- a # competition, so loAlpha < 0
hiAlpha[i,j] <- 0  # maybe there's some facilitation? who knows...
}
}
}
#........................................
# Manually adjust for known cooperations:
# (1) Wildebeest and zebra should work together.
kw <- which(species %in% c('k', 'w'))
loAlpha[kw, kw] <- 0
hiAlpha[kw, kw] <- 0.5
# (2) Elephants help facilitate feeding for some smaller animals:
fac <- which(species %in% c('v', 'r', 'wb', 'b', 'k', 'w'))
inx.o <- which(species == 'o')
loAlpha[inx.o, fac] <- loAlpha[fac, inx.o] <- 0
hiAlpha[inx.o, fac] <- hiAlpha[fac, inx.o] <- 0.5
#.............................................
alphaPrior <- list(lo = loAlpha, hi = hiAlpha)
###############################################################################
# MODEL PARAMETERS
###############################################################################
# Effort matrix
effMat[ effMat == 0 ] <- 1
effort <- list(columns = 1:S, values = effMat)
# Time and Reduct Lists
timeList <- list(times = 'year', groups = 'grid',
alphaPrior = alphaPrior, betaPrior = betaPrior,
lambdaPrior = lambdaPrior )
rl <- list(N = 8, r = 5)
# Plotting: Color species by diet type.
specColor <- rep('black', ncol(ydata))
inx.g <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'grazer')])
inx.b <- which(colnames(ydata) %in% diets$species[which(diets$diet == 'browser')])
specColor[inx.g] <- 'green'
specColor[inx.b] <- 'blue'
# Put it all together: modelList and plotPars
modelList <- list(typeNames = 'DA', ng=200, burnin=50, reductList = rl,
timeList=timeList, effort = effort)
plotPars <- list(specColor = specColor, GRIDPLOTS=T, PLOTALLY=T, SAVEPLOTS=F,
CLUSTERPLOTS=T)
###############################################################################
# RUN THE MODEL
###############################################################################
output <- gjam(bet.form, xdata=xdata, ydata=ydata, modelList=modelList)
gjamPlot(output, plotPars)
gjamPlot(output, plotPars)
gjamPlot(output, plotPars)
gjamPlot(output, plotPars)
