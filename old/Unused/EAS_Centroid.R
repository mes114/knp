# EAS_Centroid.R
# Created 08/28/19
# Author: Margaret Swift
# margaret.swift@duke.edu

# Finds centroid of EAS populations & tracks its movement over time.

#---------------------------------------------

centroidFun <- function(files,years) {
  # Start Logging
  log.file <- "C:/Users/mes114/Documents/Kruger/03_eas/0301_logs/easCentroidLog.log"
  sink(log.file, append=TRUE, split=TRUE)
  sp <- gsub('_[0-9]{4}.csv$', '', files[1])
  cat('---------------------------------\n')
  cat(toupper(sp))
  cat('\n')
  
  # Loop over years
  L <- length(files)
  c.df <- data.frame(matrix(NA, nrow=L, ncol = 5))
  names(c.df) <- c('species', 'year', 'centroid_lat', 'centroid_long', 'dispersion')
  cat("Looping over years...\n")
  
  for (i in 1:L) {
    # Read zebra data for this year, grab year and species value
    z.name <- files[i]
    z.df <- read.csv(files[i], TRUE)
    y <- findYear(z.name)
    if (nrow(z.df) == 0) {
      cat("No data for", y, '.\n')
      c.df <- deleteRow(c.df, i)  
    } else {
      # Replicate lat/lon rows by weight vector to find centroid
      z.inx <- c(which(names(z.df) == "Long"), which(names(z.df) == "Lat"))
      c.z.df <- z.df[rep(seq_len(dim(z.df)[1]), z.df$No), z.inx]
      if (nrow(z.df) > 3) { # if it's a polygon
        cent <- geosphere::centroid(c.z.df)
        disp <- dispersion(z.df)
        c.df[i,] <- c(sp, y, cent, disp)
      } else {
        cat('Insufficient data to calculate centroid for', y, '.\n')
        c.df <- deleteRow(c.df, i)  
      }
    }
  }
  
  # Find distance from starting centroid location
  cat('Calculating distances...\n')
  lat.long <- subset(c.df, select=c(centroid_long, centroid_lat))
  c.dist <- tryCatch(
    { 
      distFrom(lat.long)
    }, error = function(cond) {
      cat('Error with distance calculations!\n')
    }
  )
  n <- nrow(c.dist)
  if (!is.null(n)) {
    plot.df <- data.frame(year=c.df$year, centroid_dist=c.dist)
    miss.inx <- which(!(years %in% plot.df$year))
    if (length(miss.inx) > 0) {
      miss.df <- data.frame(year=years[miss.inx], centroid_dist=0) 
      plot.df <- rbind(plot.df, miss.df)
      plot.df <- plot.df[order(plot.df$year),] 
    }
    
    # Logging outlier years
    sd <- sd(plot.df$centroid_dist) * 1.5
    outliers <- which(plot.df$centroid_dist > sd)
    if (length(outliers) > 0) {
      cat('Unusual centroid shifts (>1.5sd) (year, dist(m)):\n')
      cat(paste(plot.df$year[outliers], plot.df$centroid_dist[outliers], collapse="\n"))
      cat('\n')
    }

    # Plotting
    cat("Plotting data...\n")
    swapPath('')
    filename <- paste('centroid_plots/', sp, '_centroid.png', sep="")
    png(filename, width = 900, height = 600)
    
    title <- paste("Distance from first year's centroid (", sp, ")", sep="")
    xlab <- "Year"
    ylab <- "Distance from Centroid(m)"
    p <- ggplot(data=plot.df, aes(x=year, y=centroid_dist)) +
      geom_bar(stat="identity", fill="steelblue") +
      ggtitle(title) + xlab(xlab) + ylab(ylab) +
      theme_grey(base_size = 18)
    
    # Save graph and close logging
    print({p})
    invisible(dev.off())
  }
  print(c.df)
  cat('ANALYSIS FINISHED.\n\n')
  sink()
}