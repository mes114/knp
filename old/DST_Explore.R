# DST_Explore.R
# Created 09/25/19
# Author: Margaret Swift
# margaret.swift@duke.edu

# Exploring KNP fixed-wing census data.
# Notes:
# -- "Rhino" is a group, I'm assuming it's just 'rhino but we
#     couldn't tell'
# -- Long/lat was switched in 2005 for some reason
# -- census for 2005-2007 didn't have a shapefile, so I used .dbf
# -- I removed any species with fewer than 60 sightings so I can just
#    focus on the ones with large enough sample sizes.

#---------------------------------------------
# Clean ws, set wd, get data.

rm(list=ls())
library(here)
source(file.path(here::here(), 'KNP_Utilities.R'))
source(file.path(dir, 'DST_Utilities.R'))
knp.border <- getKNP()

#---------------------------------------------

# easy ones
c1.df <- getShape('census_15')
c3.df <- getShape('census_22_27_2008_2010')

# hard one. 2005-2007 doesn't have a shapefile. In addition, the 2005 data
# has longitude and latitude flipped for some reason
c2.df <- foreign::read.dbf('census_22_27_2005_2007/census_22_27distance_2005_2007.dbf')
inx.2005 <- which(c2.df$YEAR == '2005')
c2.df[inx.2005,] <- swapLongLat(c2.df[inx.2005,])
c2.df <- sfFromLongLat(c2.df, "LONGITUDE", "LATITUDE")

# Clean up data: Species names and columns
c1.df.clean <- cleanData(df=c1.df, k=2, names)
c2.df.clean <- cleanData(df=c2.df, k=3, names)
c3.df.clean <- cleanData(df=c3.df, k=4, names)
c.df <- rbind(c1.df.clean, c2.df.clean, c3.df.clean)

# Plotting.
species <- unique(c.df$SPECIES)
for ( i in 1:length(species) ) {
  sp <- species[i]
  inx <- which(c.df$SPECIES == sp)
  n1 <- length(unique(c.df$CENSUS[inx])) >= 2 # if there's at least 2 entries
  n2 <- nrow(c.df[inx,]) > 60 # if there's at least 60 data points
  if (n1 && n2 && sp != "End") {
    title <- paste(sp, "occurrence in KNP\nfrom three fixed-wing surveys")
    title.key <- "Census years; \nTransect length "
    labels <- c("1998-9, 2001; 15m ", "2005-7; 22m", "2008-10; 22m")
    my.plot <- ggplot() + 
      geom_sf(data = knp.border, fill = 'white', color='transparent') +
      geom_sf(data=c.df[c.df$SPECIES==sp,], aes(col=CENSUS, fill=CENSUS)) + 
      scale_color_discrete(name=title.key, labels=labels) +
      scale_fill_discrete(name=title.key,labels=labels) +
      scale_x_continuous(breaks=c(31, 31.5, 32))  +
      ggtitle(title)
    filename <- paste(file.path(basedir, '0502_plots/occur_maps', sp), 'png', sep='.')
    #ggsave(filename, plot = last_plot())
  }
}

# Write c.df to a CSV to use later.
st_write(c.df, file.path(datadir, "all_census.shp"))
