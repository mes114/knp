# DST_Template.R
# Created 09/24/19
# Author: Margaret Swift
# margaret.swift@duke.edu

# Description

#---------------------------------------------
# Clean ws, set wd, get data.

rm(list=ls())
dir <- dirname(rstudioapi::getSourceEditorContext()$path)
utilfile <- paste(gsub('/Kruger.*', '', dir), 'Kruger/KNP_Utilities.R', sep="/")
setwd(dir)
source(utilfile)
source('DST_Utilities.R')
setwd(datadir)

#---------------------------------------------