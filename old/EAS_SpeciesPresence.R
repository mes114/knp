# EAS_SpeciesPresence.R
# Created 09/22/19
# Author: Margaret Swift
# margaret.swift@duke.edu

# Script to create a table of number of observations per 
# species, per year.

#---------------------------------------------
# Clean ws, set wd, get data.

rm(list=ls())
library(here)
source(file.path(here(), 'KNP_Utilities.R'))
source(file.path(dir, 'EAS_Utilities.R'))

#---------------------------------------------

file <- list.files(path.r)[which(grepl('EAS_ungulates', list.files(path.r)))]
all.df <- read.table(paste(path.r, file, sep="/"), header=TRUE)
sp <- unique(all.df$Sp)
years = unique(all.df$Year)
years.df <- data.frame(matrix(0, nrow=length(years), ncol=length(sp)))
row.names(years.df) <- years
names(years.df) <- sp
for (i in 1:length(years)) {
  year <- as.character(years[i])
  df <- all.df[all.df$Year==year, ]
  for (j in 1:length(sp)) {
    species <- as.character(sp[j])
    inx <- which(df$Sp==species)
    years.df[i,j] <- length(inx)
  }
}
write.csv(years.df,paste(path.r, "yearly_species_observations.csv", sep="/"))
