http://dataknp.sanparks.org/sanparks/metacat

Title 			Kruger National Park Landscapes
ID 			judith_kruger.16.1 
Originator  		KNP GIS Lab 
Publication Date 	2001 01 01 
Edition 		February 2001 

Abstract 		This is a multipart polygon theme indicating the extent of 
			Lanscapes in the Kruger National Park (KNP) according to Gertenbach 
			(1983). The reference can be viewed online: 
			http://www.koedoe.co.za/index.php/koedoe/article/viewArticle/591. 
			The KNP falls under the authority of South African National Parks 
			(http://www.sanparks.org).

Purpose 		For the KNP Scientific Services GIS database - for research and 
			cartograhic products. This database is intended to be extended or 
			incorporated into a future KNP "Regional GIS" node of SANP 
