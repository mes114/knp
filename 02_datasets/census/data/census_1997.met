IDENTIFICATION_INFORMATION

  Citation:
    Citation_Information:
      Originator: KNP GIS Lab
      Publication_Date: 20020101
      Title: Elephant, Buffalo and Rhino census results for 1997 in the
        Kruger National Park (KNP)
      Edition: January 1998
      Geospatial_Data_Presentation_Form: Map
      Publication_Information:
        Publication_Place: Skukuza, Kruger National Park
        Publisher: KNP GIS Lab
      Other_Citation_Details:
        This data layer was digitized using the SummaSketch
        Professional digitizing tablet from 1:100 000 hardcopy maps
        of the Kruger National Park used during the annual
        megaherbivore census.
      Online_Linkage: 
      Larger_Work_Citation:
        Citation_Information:
          Originator: 
          Publication_Date: 
          Title: 
          Publication_Information:
            Publication_Place: 
            Publisher: 
          Online_Linkage: 
  Description:
    Abstract:
      This is a point theme indicating the 1997 distribution of
      Elephant, Buffalo, Rhino and rare game in the Kruger
      National Park (KNP). The KNP falls under the authority of
      South African National Parks (www.parks-sa.co.za).
    Purpose:
      For the KNP Scientific Services GIS database - for research
      and cartograhic products. This database is intended to be
      extended or incorporated into a future KNP "Regional GIS"
      node of SANP.
    Supplemental_Information:
  Time_Period_of_Content:
    Time_Period_Information:
      Range_of_Dates/Times:
        Beginning_Date: Unknown
        Ending_Date: Unknown
    Currentness_Reference: Unknown
  Status:
    Progress: Complete
    Maintenance_and_Update_Frequency: Continually
  Spatial_Domain:
    Bounding_Coordinates:
      West_Bounding_Coordinate: 30.8787
      East_Bounding_Coordinate: 32.0495
      North_Bounding_Coordinate: -22.3597
      South_Bounding_Coordinate: -25.5307
  Keywords:
    Theme:
      Theme_Keyword_Thesaurus: None
      Theme_Keyword: Kruger National Park
      Theme_Keyword: Elephant
      Theme_Keyword: Bufalo
      Theme_Keyword: Rhino
      Theme_Keyword: Census
      Theme_Keyword: 1997
    Place:
      Place_Keyword_Thesaurus: None
      Place_Keyword: Kruger National Park
      Place_Keyword: Mpumalanga
      Place_Keyword: Northern Province
      Place_Keyword: South Africa
  Access_Constraints:
    None
  Use_Constraints:
    Use at own risk. See Liability and Accuracy sections
  Point_of_Contact:
    Contact_Information:
      Contact_Organization_Primary:
        Contact_Organization: South African National Parks
        Contact_Person: Don Ntsala
      Contact_Position: KNP Scientific Services GIS Administrator
      Contact_Address:
        Address_Type: mailing and physical address
        Address: Kruger National Park, Private Bag X402
        City: Skukuza
        State_or_Province: Mpumalanga
        Postal_Code: 1350
        Country: South Africa
      Contact_Voice_Telephone: +27 (0)13 735-4236
      Contact_Facsimile_Telephone: +27 (0)13 735-4055
      Contact_Electronic_Mail_Address: gismaster@parks-sa.co.za
      Hours_of_Service: Monday - Friday 07:00 - 12:45, 13:45 - 16:00 local time
  Native_Data_Set_Environment:
    ArcView version 3.2 shapefile format
    f:\knp_data_utm\fauna\megaherbivore_census\eb97.shp
 
DATA_QUALITY_INFORMATION

  Attribute_Accuracy:
    Attribute_Accuracy_Report:
      Not applicable
  Logical_Consistency_Report:
    Not Applicable
  Completeness_Report:
    Complete.
  Positional_Accuracy:
    Horizontal_Positional_Accuracy:
      Horizontal_Positional_Accuracy_Report:
        Believed to be within 50m of the true location
    Vertical_Positional_Accuracy:
      Vertical_Positional_Accuracy_Report:
        Not applicable.
  Lineage:
    Source_Information:
      Source_Citation:
        Citation_Information:
          Originator: Kruger National Park Scientific Services GIS Lab
          Publication_Date: 20020101
          Title: Elephant, Buffalo, Rhino and rare game census results for
            1997 in the Kruger National Park
          Edition: January 1998
          Geospatial_Data_Presentation_Form: map
          Publication_Information:
            Publication_Place: Skukuza
            Publisher: KNP GIS Lab
          Other_Citation_Details:
            Digitized using the SummaSketch Professional digitizing
            tablet from 1:100 000 hardcopy maps of the Kruger National
            Park used during the annual megaherbivore census.
          Online_Linkage: 
          Larger_Work_Citation:
            Citation_Information:
              Originator: 
              Publication_Date: 
              Title: 
              Publication_Information:
                Publication_Place: 
                Publisher: 
              Online_Linkage: 
      Source_Scale_Denominator: 1:100 000
      Type_of_Source_Media: Mapsheet
      Source_Time_Period_of_Content:
        Time_Period_Information:
          Range_of_Dates/Times:
            Beginning_Date: Unknown
            Ending_Date: Unknown
        Source_Currentness_Reference: Unknown
      Source_Citation_Abbreviation: 
      Source_Contribution:
    Process_Step:
      Process_Description:
        Digitized using the SummaSketch Professional digitizing
        tablet from 1:100 000 hardcopy maps, codes divided into
        separate fields
        for analysis.
      Source_Used_Citation_Abbreviation: 
      Process_Date: 
      Source_Produced_Citation_Abbreviation: 
      Process_Contact:
        Contact_Information:
          Contact_Person_Primary:
            Contact_Organization: South African National Parks
            Contact_Person: Don Ntsala
          Contact_Position: KNP Scientific Services GIS Administrator
          Contact_Address:
            Address_Type: mailing and physical address
            Address: Kruger National Park, Private Bag X402
            City: Skukuza
            State_or_Province: Mpumalanga
            Postal_Code: 1350
            Country: South Africa
          Contact_Voice_Telephone: +27 (0)13 735-4236
          Contact_Facsimile_Telephone: +27 (0)13 735-4055
          Contact_Electronic_Mail_Address: gismaster@parks-sa.co.za
          Hours_of_Service: Monday - Friday 07:00 - 12:45, 13:45 - 16:00 local time

SPATIAL_DATA_ORGANIZATION_INFORMATION

  Direct_Spatial_Reference_Method: Point
  Point_and_Vector_Object_Information:
    SDTS_Terms_Description:
      SDTS_Point_and_Vector_Object_Type: Point
      Point_and_Vector_Object_Count: 2595

SPATIAL_REFERENCE_INFORMATION
 
  Horizontal_Coordinate_System_Definition:
    Planar:
      Map_Projection:
        Map_Projection_Name: Transverse Mercator
        Transverse_Mercator:
          Scale_Factor_at_Central_Meridian: 0.999600
          Longitude_of_Central_Meridian: 33.000000
          Latitude_of_Projection_Origin: 0.000000
          False_Easting: 500000.000000
          False_Northing: 10000000.000000
      Planar_Coordinate_Information:
        Planar_Coordinate_Encoding_Method: Row and column
        Coordinate_Representation:
          Abscissa_Resolution: 
          Ordinate_Resolution: 
        Planar_Distance_Units: Meters
    Geodetic_Model:
      Horizontal_Datum_Name: Hartebeeshoek 94
      Ellipsoid_Name: WGS 84
      Semi-major_Axis: 6378137.000
      Denominator_of_Flattening_Ratio: 298.25722

ENTITY_AND_ATTRIBUTE_INFORMATION

  Detailed_Description:
    Entity_Type:
      Entity_Type_Label: eb97.dbf
      Entity_Type_Definition: Shapefile Attribute Table
      Entity_Type_Definition_Source: None
    Attribute:
      Attribute_Label: Year
      Attribute_Definition: Census year
      Attribute_Definition_Source: none
      Attribute_Domain_Values:
        Unrepresentable_Domain: Character Field
    Attribute:
      Attribute_Label: Sp
      Attribute_Definition: Species code
      Attribute_Definition_Source: none
      Attribute_Domain_Values:
        Unrepresentable_Domain: Character Field
    Attribute:
      Attribute_Label: No
      Attribute_Definition: Animal totals
      Attribute_Definition_Source: none
      Attribute_Domain_Values:
        Unrepresentable_Domain: Numeric Field
    Attribute:
      Attribute_Label: Lat
      Attribute_Definition: Latitude
      Attribute_Definition_Source: none
      Attribute_Domain_Values:
        Unrepresentable_Domain: Numeric Field
    Attribute:
      Attribute_Label: Lon
      Attribute_Definition: Longitude
      Attribute_Definition_Source: none
      Attribute_Domain_Values:
        Unrepresentable_Domain: Numeric Field
    Attribute:
      Attribute_Label: Cenblock
      Attribute_Definition: Census block ID
      Attribute_Definition_Source: Census_blocks
      Attribute_Domain_Values:
        Unrepresentable_Domain: Numeric Field

DISTRIBUTION_INFORMATION

  Distributor:
    Contact_Information:
      Contact_Organization_Primary:
        Contact_Organization: South African National Parks
        Contact_Person: Don Ntsala
      Contact_Position: KNP Scientific Services GIS Administrator
      Contact_Address:
        Address_Type: mailing and physical address
        Address: Kruger National Park, Private Bag X402
        City: Skukuza
        State_or_Province: Mpumalanga
        Postal_Code: 1350
        Country: South Africa
      Contact_Voice_Telephone: +27 (0)13 735-4236
      Contact_Facsimile_Telephone: +27 (0)13 735-4055
      Contact_Electronic_Mail_Address: gismaster@parks-sa.co.za
      Hours_of_Service: Monday - Friday 07:00 - 12:45, 13:45 - 16:00 local time
  Resource_Description:
    KNP SS GIS database
  Distribution_Liability:
    Users must assume responsibility to determine appropriate
    use of these data. The data were gathered for scientific
    use and do not conform to survey accuracy. The data are
    provided "as-is" to the public, especially intended for
    researchers and the KNP GIS Lab may not be held liable for
    any loss or damage incurred due to inaccuracy of the data.
  Custom_Order_Process:
    Send an e-mail request to gismaster@parks-sa.co.za or phone
    during office hours +27 (0)13 735-4257 to request these
    data in digital form. Nominal fees are charged for data
    conversion, storage medium, packaging and postage if the
    requested data are practically too large to send as e-mail attachments.

METADATA_REFERENCE_INFORMATION

  Metadata_Date: 20020117
  Metadata_Review_Date: 20020117
  Metadata_Contact:
    Contact_Information:
      Contact_Organization_Primary:
        Contact_Organization: South African National Parks
        Contact_Person: Don Ntsala
      Contact_Position: KNP Scientific Services GIS Administrator
      Contact_Address:
        Address_Type: Mailing and physical address
        Address: Kruger National Park, Private Bag X402
        City: Skukuza
        State_or_Province: Mpumalanga
        Postal_Code: 1350
        Country: South Africa
      Contact_Voice_Telephone: +27 (0)13 735-4236
      Contact_Facsimile_Telephone: +27 (0)13 735-4055
      Contact_Electronic_Mail_Address: gismaster@parks-sa.co.za
      Hours_of_Service: Monday - Friday 07:00 - 12:45, 13:45 - 16:00 local time
  Metadata_Standard_Name: FGDC CSDGM
  Metadata_Standard_Version: FGDC-STD-001-1998
