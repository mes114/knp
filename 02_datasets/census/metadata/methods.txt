Helicopter mega-herbivore census Methods

Smit, I.P.J, Grant, C.C., Whyte, I.J. (2007)
Landscape-scale sexual segregation in the dry season distribution 
and resource utilization of elephants in Kruger National Park, 
South Africa. Diversity and Distributions 13, 225-236.

"A helicopter was flown at an average height of 150 m and at a
speed of 100 knots, with four observers on board, counting and
noting all elephants, buffaloes and rhinos. These annual censuses
were conducted over the whole park in the late dry season
(August), using a total count on a pattern that follows all of the
drainage lines (Whyte, 2001a). When a big elephant herd was
sighted, the helicopter would circle lower until the observers
reached consensus on the herd size. The number and location
was plotted on a 1 : 100,000 map for later digitization. For each
sighting, it was noted whether it was a mixed herd or a bull
group. Mixed herds were defined to include breeding herds with
a matriarch and other adult females with their offspring, as well
as family units attended by adult males. Bull groups comprised of
adult males only, occurring independently from family units.
Individual elephant bulls were included in the bull group category.
We used data from 1985 to 1996 in this study, with census
totals ranging from 6887 to 8371 individuals over this period. No
accurate locations were recorded for the sightings prior to 1985.
In 1997, the water provision policy in the park was revised and
many artificial waterholes were closed (Pienaar et al., 1997). This
caused a change in the resource distribution and hence data from
post 1996 were excluded from the present analysis.
In order to get a better idea of elephant distribution, all sightings
over the 12-year period were combined into a single GIS
data layer. This approach of combining snap-shot data from
various periods is common in ecological studies (e.g. Osborne
et al., 2001; Lichstein et al., 2002; Silva et al., 2002; Tognelli &
Kelt, 2004; Olivera-Gomez & Mellink, 2005). The data were then
transformed into absence/presence data for each 1-km grid cell."