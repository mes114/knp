http://dataknp.sanparks.org/sanparks/metacat

Title 			Kruger National Park ecozones
ID 			judith_kruger.12.1 
Originator  		KNP GIS Lab 
Publication Date 	2001 01 01 
Edition 		February 2001 

Abstract 		This is a single polygon theme indicating the extent of 
			Ecozones (Jacana 1991) in the Kruger National Park (KNP). The KNP falls under the 
			authority of South African National Parks (http://www.sanparks.org). 

Purpose 		For the KNP Scientific Services GIS database - for research and 
			cartograhic products. This database is intended to be extended or 
			incorporated into a future KNP "Regional GIS" node of SANP 


ECOZONE DESCRIPTIONS:
http://home.intekom.com/ecotravel/Guides/Reserves/KNP/EcoZones/Mixed_Bushwillow_Woodlands.htm

A: Mixed Bushwillow Woodlands 	-- Description: The Mixed Bushwillow Woodlands Ecozone is found on underlying Granite bedrock giving rise to gently sloping hills and rocky outcrops with huge round boulders such as at the Stevenson Hamilton Memorial. Broad-leaved trees favour the higher upland areas while thorn trees prefer the lower-lying areas. Many rivers and streams are found in this region.

B: Pretoriuskop Sourveld 	-- Description: The gently sloping hills and valleys of the Pretoriuskop Sourveld Ecozone are covered with dense trees, shrubs and tall grass. According to historical records this area did not have many trees in the days before Europeans settled here and started farming the land. The area is scattered with impressive granite hills and koppies such as Shabeni and Manungu.

C: Malelane Mountain Bushveld 	-- Description: The Malelane Mountain Bushveld Ecozone in the Kruger National Park is found on underlying Granite bedrock. The ecozone is restricted to the mountains near Berg-en-dal and Malelane.
D: Sabie/Crocodile Thorn Thickets -- Description: The Sabie / Crocodile Thorn Thickets ecozone is relatively flat with a very dense growth of thorny shrubs. This is typical of the area around Skukuza.
E: Thorn Veld 			-- Description: The Thorn Veld Ecozone in Kruger National Park is found [ nothing more after this :( ]
F: Knob Thorn/Marula Savannah 	-- Description: The Knob Thorn / Marula Savannah Ecozone in the Kruger National Park is found on dark Basalt clays. This typical savannah ecozone with its sweet grasses attracts large herds of plains game such as zebra and wildebeest. It is also home to one of the parks largest Lion prides whose territory is in the vicinity of the Sweni River.
 G: Delagoa Thorn Thickets 	-- Description: There is often abundant game in this ecozone as the grasses are very sweet. The dense thickets of Delagoa thorn trees are often interspersed with bare overgrazed patches and muddy depressions after rain.

H: Riverine Communities 	-- Description: Riverine communities are formed alongside rivers and cross through many other landscapes. You will see many different animals coming down to drink along the rivers.
I: Lebombo Mountain Bushveld 	-- Description: The rugged Lebombo mountains range over the whole length of the Kruger National Park and as far south as The Greater St Lucia Wetland Park. The many rocky outcrops and deep ravines are a result of the underlying Rhyolite bedrock which is resistant to weathering.

J: Olifants Rugged Veld 	-- Description: Olifants Rugged Veld Ecozone occurs on Basalt and Rhyolite bedrock. The round Basalt boulders stewn accross the veld can give this area a very harsh apperance at times hence the apt Ecozone name "Olifants Rugged Veld"! After good rains many annual plants and grasses will fill this once "barren" landscape with a lush carpet of green.
K: Stunted Knob Thorn Savannah 	-- Description: The Stunted Knob Thorn Savannah Ecozone in Kruger National Park is found [ nothing more after this :( ]

L: Mopane Shrubveld 		-- Description: Typical of Basalt soils the Mopane Scrubveld Ecozone is very flat with very few drainage lines. It typically has areas of dense mopane shrub and sparse grass cover.
M: Alluvial Plains 		-- Description: The Alluvial Plains Ecozone in Kruger National Park is found along the main rivers and water courses. The Alluvial Plains were formed and are still being formed by rivers depositing soils during floods.
N: Sandveld Communities 	-- Description: The Sandveld in Kruger is found mainly in the north. The vegetation is complex with several tree species that are unique to the park.
          O: Tree Mopane Savannah 	-- Description: The Tree Mopane Savannah Ecozone is formed on highly erosive Ecca Shale bedrock and is domminated [sic] by tall Mopane. The area is very flat and water does not penetrate very easily causing many pans to be formed.
       
P: Mopane/Bushwillow Woodlands 	-- Description: The Mopane Bushwillow Woodlands Ecozone in Kruger National Park is found on underlying Granite bedrock with sandy soils on the crests and gradually becoming clay at the drainage line. This is a dominant ecozone in the north-western section of the park.

  
  


